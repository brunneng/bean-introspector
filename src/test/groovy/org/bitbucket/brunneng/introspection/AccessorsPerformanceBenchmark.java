package org.bitbucket.brunneng.introspection;

import org.junit.jupiter.api.Test;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@OperationsPerInvocation(10000)
public class AccessorsPerformanceBenchmark {

   private Order order;
   private Customer otherCustomer;
   private Method getCustomerMethod;
   private MethodHandle getCustomerMethodHandle;
   private MethodHandle getCustomerMethodHandleObject;
   private Method setCustomerMethod;
   private MethodHandle setCustomerMethodHandle;

   public static void main(String[] args) throws RunnerException {
      Options opt = new OptionsBuilder().include(AccessorsPerformanceBenchmark.class.getSimpleName())
            .forks(1)
            .threads(10)
            .warmupIterations(2)
            .measurementIterations(2)
            .build();

      new Runner(opt).run();
   }

   @Setup
   public void setup() throws NoSuchMethodException, IllegalAccessException {
      order = buildOrder();
      getCustomerMethod = Order.class.getMethod("getCustomer");
      getCustomerMethodHandle = MethodHandles.lookup().unreflect(getCustomerMethod);
      getCustomerMethodHandleObject = getCustomerMethodHandle.asType(MethodType.methodType(Object.class, Object.class));

      setCustomerMethod = Order.class.getMethod("setCustomer", Customer.class);
      setCustomerMethodHandle = MethodHandles.lookup().unreflect(setCustomerMethod);
   }

   static Order buildOrder() {
      Order res = new Order();
      res.setCustomer(new Customer());
      res.setArray(new Integer[]{1, 2});
      return res;
   }

//   @Benchmark
//   public Customer getCustomerPlain() {
//      return order.getCustomer();
//   }
//
//   @Benchmark
//   public Customer getCustomerReflection() throws Exception {
//      return (Customer) getCustomerMethod.invoke(order);
//   }
//
//   @Benchmark
//   public Customer getCustomerMethodHandleInvokeExact() throws Throwable {
//      return (Customer) getCustomerMethodHandle.invokeExact(order);
//   }

   @Benchmark
   public Customer getCustomerMethodHandleInvokeExactObject() throws Throwable {
      Object res = getCustomerMethodHandleObject.invokeExact((Object)order);
      return (Customer)res;
   }

//   @Benchmark
//   public Customer getCustomerMethodHandleInvokeWithArgs() throws Throwable {
//      return (Customer) getCustomerMethodHandle.invokeWithArguments(order);
//   }
//
//   @Benchmark
//   public Customer getCustomerMethodHandleInvoke() throws Throwable {
//      return (Customer) getCustomerMethodHandle.invoke(order);
//   }
//
//   @Benchmark
//   public void setCustomerPlain() {
//      order.setCustomer(otherCustomer);
//   }
//
//   @Benchmark
//   public void setCustomerReflection() throws Exception {
//      setCustomerMethod.invoke(order, otherCustomer);
//   }
//
//   @Benchmark
//   public void setCustomerMethodHandleInvokeExact() throws Throwable {
//      setCustomerMethodHandle.invokeExact(order, otherCustomer);
//   }
//
//   @Benchmark
//   public void setCustomerMethodHandleInvokeWithArgs() throws Throwable {
//      setCustomerMethodHandle.invokeWithArguments(order, otherCustomer);
//   }
//
//   @Benchmark
//   public void setCustomerMethodHandleInvoke() throws Throwable {
//      setCustomerMethodHandle.invoke(order, otherCustomer);
//   }

   @Test
   public void testStrictMethodHandle() throws Throwable {
      order = buildOrder();
      getCustomerMethod = Order.class.getMethod("getCustomer");
      getCustomerMethodHandle = MethodHandles.lookup().unreflect(getCustomerMethod)
            .asType(MethodType.methodType(Object.class, Object.class));

      System.out.println((Object) getCustomerMethodHandle.invokeExact((Object) order));

      Order o1 = new Order();
      Method arraySetter1 = Order.class.getMethod("setArray", Integer[].class);
      MethodHandle arrayHandle1 = MethodHandles.lookup().unreflect(arraySetter1)
            .asType(MethodType.methodType(void.class, Object.class, Object.class));

      arrayHandle1.invokeExact((Object) o1, (Object) new Integer[] {2, 3});

      Method arraySetter2 = Order.class.getMethod("setArrayVarargs", Integer[].class);
      MethodHandle arrayHandle2 = MethodHandles.lookup().unreflect(arraySetter2).asFixedArity()
            .asType(MethodType.methodType(void.class, Object.class, Object.class));

      // To deal with varargs asFixedArity() is used.

      arrayHandle2.invokeExact((Object) o1, (Object) new Integer[] {2, 3});
   }

   public static class Order {
      private Customer customer;
      private Integer[] array;

      public Customer getCustomer() {
         return customer;
      }
      public void setCustomer(Customer customer) {
         this.customer = customer;
      }

      public Integer[] getArray() {
         return array;
      }
      public void setArray(Integer[] array) {
         this.array = array;
      }

      public void setArrayVarargs(Integer... array) {
         this.array = array;
      }
   }

   public static class Customer {
   }
}
