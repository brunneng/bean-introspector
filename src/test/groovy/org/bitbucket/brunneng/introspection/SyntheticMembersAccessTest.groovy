package org.bitbucket.brunneng.introspection

import org.bitbucket.brunneng.introspection.package1.SyntheticTestBean
import spock.lang.Specification

/**
 * @author evvo
 */
class SyntheticMembersAccessTest extends Specification {

    def 'test getter'() {
        when:
        Introspector i = new Introspector()
        then:
        SyntheticTestBean.class.getMethod("getSize").isSynthetic()
        i.beanOfClass(SyntheticTestBean.class).property("size").getter().fieldOrMethod.isSynthetic()
    }
}
