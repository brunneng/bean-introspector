package org.bitbucket.brunneng.introspection

import org.bitbucket.brunneng.introspection.Introspector
import spock.lang.Specification

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

/**
 * @author evvo
 */
class GetBeanAnnotationsTest extends Specification {

    def 'test get bean annotations'() {
        when:
        def i = new Introspector()
        def annotations = i.beanOfClass(Bean1.class).annotations
        then:
        annotations.size() == 4
        annotations[0] instanceof TestAnnotation1
        ((TestAnnotation1)annotations[0]).value() == "v1"
        annotations[1] instanceof TestAnnotation2
        annotations[2] instanceof TestAnnotation1
        ((TestAnnotation1)annotations[2]).value() == "v2"
        annotations[3] instanceof TestAnnotation3
    }

    @TestAnnotation1("v1")
    @TestAnnotation2
    class Bean1 extends Bean1Parent {
    }

    @TestAnnotation1("v2")
    @TestAnnotation2
    @TestAnnotation3
    class Bean1Parent {
    }
}

@Target(value=[ElementType.TYPE])
@Retention(RetentionPolicy.RUNTIME)
@interface TestAnnotation1 {
    String value()
}

@Target(value=[ElementType.TYPE])
@Retention(RetentionPolicy.RUNTIME)
@interface TestAnnotation2 {
}

@Target(value=[ElementType.TYPE])
@Retention(RetentionPolicy.RUNTIME)
@interface TestAnnotation3 {
}

