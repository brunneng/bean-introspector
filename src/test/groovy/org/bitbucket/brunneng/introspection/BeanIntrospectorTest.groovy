package org.bitbucket.brunneng.introspection

import org.bitbucket.brunneng.introspection.exceptions.NoSuchPropertyException
import org.bitbucket.brunneng.introspection.exceptions.WrongBeanClassException
import spock.lang.Specification

import java.util.stream.Collectors

class BeanIntrospectorTest extends Specification {
  def 'test bean has property'() {
    when:
    def i = new Introspector()
    def bean = i.beanOfClass(Bean1.class)
    then:
    bean.hasProperty("v1")
    !bean.hasProperty("tost")
  }

  def 'test bean get property'() {
    when:
    def i = new Introspector()
    def bean = i.beanOfClass(Bean1.class)
    then:
    bean.property("v1") != null
  }

  def 'test bean get property - not found'() {
    when:
    def i = new Introspector()
    def bean = i.beanOfClass(Bean1.class)
    bean.property("tost")
    then:
    thrown NoSuchPropertyException.class
  }

  def 'test bean get properties if detectReferenceToParentOfNonStaticInnerClass == true'() {
    when:
    def i = new Introspector()
    i.accessorsDetectionOptions.detectReferenceToParentOfNonStaticInnerClass = true
    def bean = i.beanOfClass(Bean1.class)
    then:
    bean.getProperties().stream().map({ p -> p.getName() }).collect(Collectors.toList()) ==
            ['this$0', "v1", "v2", "v3", "v4", "v5", "v6"]
  }

  def 'test bean get properties'() {
    when:
    def i = new Introspector()
    def bean = i.beanOfClass(Bean1.class)
    then:
    bean.getProperties().stream().map({ p -> p.getName() }).collect(Collectors.toList()) ==
            ["v1", "v2", "v3", "v4", "v5", "v6"]
  }

  def 'test bean get properties with getter'() {
    when:
    def i = new Introspector()
    def bean = i.beanOfClass(Bean1.class)
    then:
    bean.getPropertiesWithGetter().stream().map({ p -> p.getName() }).collect(Collectors.toList()) ==
            ["v1", "v2", "v3", "v4"]
  }

  def 'test bean get properties with public getter'() {
    when:
    def i = new Introspector()
    def bean = i.beanOfClass(Bean1.class)
    then:
    bean.getPropertiesWithPublicGetter().stream().map({ p -> p.getName() }).collect(Collectors.toList()) ==
            ["v1", "v3"]
  }

  def 'test bean get properties with setter'() {
    when:
    def i = new Introspector()
    def bean = i.beanOfClass(Bean1.class)
    then:
    bean.getPropertiesWithSetter().stream().map({ p -> p.getName() }).collect(Collectors.toList()) ==
            ["v1", "v2", "v5", "v6"]
  }

  def 'test bean get properties with public setter'() {
    when:
    def i = new Introspector()
    def bean = i.beanOfClass(Bean1.class)
    then:
    bean.getPropertiesWithPublicSetter().stream().map({ p -> p.getName() }).collect(Collectors.toList()) ==
            ["v1", "v5"]
  }

  def 'test bean get parent bean'() {
    when:
    def i = new Introspector()
    then:
    i.beanOfClass(Bean1.class).parentBeanIntrospector == i.beanOfClass(Bean1Parent.class)
    i.beanOfClass(Bean1Parent.class).parentBeanIntrospector == null
  }

  def 'test get bean wrong class'() {
    when:
    def i = new Introspector()
    i.beanOfClass(String.class)
    then:
    def ex = thrown WrongBeanClassException
    ex.message == "class java.lang.String is value type, not a bean"
  }

  class Bean1 extends Bean1Parent {
    String v1
    private String v2

    String getV3() {
      return "v3"
    }

    private boolean isV4() {
      return true
    }

    void setV5(String value) {

    }

    private void setV6(String value) {

    }

  }

  class Bean1Parent {

  }
}
