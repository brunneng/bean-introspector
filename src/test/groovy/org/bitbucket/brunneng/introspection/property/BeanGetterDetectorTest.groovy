package org.bitbucket.brunneng.introspection.property

import org.bitbucket.brunneng.introspection.exceptions.GetPropertyValueException
import spock.lang.Specification

class BeanGetterDetectorTest extends Specification {

  def 'test find and use bean getters'(List<Object> values) {
    when:
    def detector = new BeanGetterDetector()
    def a = new Bean1(values[0], values[1], values[2])
    List<Getter> getters = detector.detectAccessors(a.getClass(), new AccessorsDetectionOptions())
    def properties = ["i", "s", "d"]
    then:
    getters.size() == properties.size()
    for (def fieldWithIndex : properties.withIndex()) {
      def fieldName = fieldWithIndex.first
      def index = fieldWithIndex.second
      getters.stream().filter { g -> g.propertyName == fieldName }.findFirst().get().get(a) == values[index]
    }
    where:
    values            | _
    [1, "s", 1.0]     | _
    [17, "jdkf", 0.0] | _
    [-1, null, -1.0]  | _
  }

  def 'test get property name'(String getterName, String propertyName) {
    when:
    def detector = new BeanGetterDetector()
    then:
    detector.getPropertyName(getterName) == propertyName
    where:
    getterName | propertyName
    "getTest"  | "test"
    "getI"     | "i"
    "getBB"    | "BB"
    "getBb"    | "bb"
    "getA1"    | "a1"
    "getA_1"   | "a_1"
  }

  def 'test get property exception message'() {
    when:
    def detector = new BeanGetterDetector()
    def a = new BeanThrowingExceptions()
    List<Getter> getters = detector.detectAccessors(a.getClass(), new AccessorsDetectionOptions())
    then:
    getters.size() == 1
    when:
    getters.get(0).get(a)
    then:
    GetPropertyValueException e = thrown()
    e.message == "Failed to get property: value12"
    e.property == "value12"
    e.srcBean == a
  }
}
