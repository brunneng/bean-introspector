package org.bitbucket.brunneng.introspection.property

import org.bitbucket.brunneng.introspection.Introspector
import spock.lang.Specification

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

/**
 * @author evvo
 */
class GetPropertyAnnotationsTest extends Specification {

    def 'test get property annotations'() {
        when:
        def i = new Introspector()
        def annotations = i.beanOfClass(Bean1.class).property("v7").annotations
        then:
        annotations.size() == 4
        annotations[0] instanceof TestAnnotation1
        ((TestAnnotation1)annotations[0]).value() == "v2"
        annotations[1] instanceof TestAnnotation2
        annotations[2] instanceof TestAnnotation1
        ((TestAnnotation1)annotations[2]).value() == "v1"
        annotations[3] instanceof TestAnnotation3
    }

    def 'test no annotations'() {
        when:
        def i = new Introspector()
        def annotations = i.beanOfClass(Bean1.class).property("v8").annotations
        then:
        annotations.size() == 0
    }

    class Bean1 extends Bean1Parent {
        private String v8

        @TestAnnotation2
        void setV7(String value) {
            v7 = value
        }
    }

    class Bean1Parent {
        @TestAnnotation1("v1")
        @TestAnnotation3
        protected String v7

        @TestAnnotation1("v2")
        String getV7() {
            return v7
        }
    }
}

@Target(value=[ElementType.METHOD, ElementType.FIELD])
@Retention(RetentionPolicy.RUNTIME)
@interface TestAnnotation1 {
    String value()
}

@Target(value=[ElementType.METHOD, ElementType.FIELD])
@Retention(RetentionPolicy.RUNTIME)
@interface TestAnnotation2 {
}

@Target(value=[ElementType.METHOD, ElementType.FIELD])
@Retention(RetentionPolicy.RUNTIME)
@interface TestAnnotation3 {
}
