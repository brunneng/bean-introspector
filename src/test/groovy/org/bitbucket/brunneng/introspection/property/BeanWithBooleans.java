package org.bitbucket.brunneng.introspection.property;

public class BeanWithBooleans {

   private final boolean b1;
   private final Boolean b2;
   private String s;

   public BeanWithBooleans(boolean b1, Boolean b2) {
      this.b1 = b1;
      this.b2 = b2;
   }

   public boolean isB1() {
      return b1;
   }

   public Boolean isB2() {
      return b2;
   }

   public String getS() {
      return s;
   }

   public boolean is() {
      return false;
   }
}
