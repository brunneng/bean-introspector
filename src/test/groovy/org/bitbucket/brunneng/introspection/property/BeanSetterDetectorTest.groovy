package org.bitbucket.brunneng.introspection.property

import org.bitbucket.brunneng.introspection.exceptions.SetPropertyValueException
import spock.lang.Specification

class BeanSetterDetectorTest extends Specification {


  def 'test find and use bean setters'(List<Object> values) {
    when:
    def detector = new BeanSetterDetector()
    def a = new Bean1()
    List<Setter> setters = detector.detectAccessors(a.getClass(), new AccessorsDetectionOptions())
    def properties = ["i", "s"]
    then:
    setters.size() == properties.size()
    for (def fieldWithIndex : properties.withIndex()) {
      def fieldName = fieldWithIndex.first
      def index = fieldWithIndex.second
      def setter = setters.stream().filter { g -> g.propertyName == fieldName }.findFirst().get()

      def value = values[index]
      if (value != null) {
        assert setter.getAccessorValueType().actualClass.isAssignableFrom(value.getClass())
      }
      setter.set(a, value)
    }

    a.i == values.get(0)
    a.s == values.get(1)
    where:
    values       | _
    [5, "a"]     | _
    [17, "kdjf"] | _
    [-1, null]   | _
  }

  def 'test set property exception message'() {
    when:
    def detector = new BeanSetterDetector()
    def a = new BeanThrowingExceptions()
    List<Setter> setters = detector.detectAccessors(a.getClass(), new AccessorsDetectionOptions())
    then:
    setters.size() == 1
    when:
    setters.get(0).set(a, "v")
    then:
    SetPropertyValueException e = thrown()
    e.message == "Failed to set property: value12"
    e.property == "value12"
    e.destBean == a
  }
}
