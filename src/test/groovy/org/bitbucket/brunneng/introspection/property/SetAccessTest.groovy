package org.bitbucket.brunneng.introspection.property

import org.bitbucket.brunneng.introspection.Introspector
import org.bitbucket.brunneng.introspection.property.AccessorsTestBean
import spock.lang.Specification

import java.lang.reflect.Method

/**
 * @author evvo
 */
class SetAccessTest extends Specification {

    def 'test get property getter() & onlyPublic'(String propertyName, boolean onlyPublic, Boolean methodAccessor) {
        when:
        def i = new Introspector()
        def propertyConfig = i.beanOfClass(AccessorsTestBean.class).property(propertyName)
        if (onlyPublic) {
            propertyConfig.allowOnlyPublicAccessors()
        }
        then:
        def getter = propertyConfig.getter()
        if (methodAccessor == null) {
            assert getter == null
        } else {
            assert getter.fieldOrMethod instanceof Method == methodAccessor
        }
        where:
        propertyName | onlyPublic | methodAccessor
        "s1"         | false      | false
        "s1"         | true       | null
        "s2"         | false      | false
        "s2"         | true       | false
        "s3"         | false      | true
        "s3"         | true       | true
        "s4"         | false      | false
        "s4"         | true       | false
        "s5"         | false      | true
        "s5"         | true       | true
        "s6"         | false      | true
        "s6"         | true       | null
    }

    def 'test get property setter() & onlyPublic'(String propertyName, boolean onlyPublic, Boolean methodAccessor) {
        when:
        def i = new Introspector()
        def propertyConfig = i.beanOfClass(AccessorsTestBean.class).property(propertyName)
        if (onlyPublic) {
            propertyConfig.allowOnlyPublicAccessors()
        }
        then:
        def setter = propertyConfig.setter()
        if (methodAccessor == null) {
            assert setter == null
        } else {
            assert setter.fieldOrMethod instanceof Method == methodAccessor
        }
        where:
        propertyName | onlyPublic | methodAccessor
        "s1"         | false      | false
        "s1"         | true       | null
        "s2"         | false      | false
        "s2"         | true       | false
        "s3"         | false      | true
        "s3"         | true       | true
        "s4"         | false      | false
        "s4"         | true       | false
        "s5"         | false      | true
        "s5"         | true       | true
        "s6"         | false      | true
        "s6"         | true       | null
    }


}
