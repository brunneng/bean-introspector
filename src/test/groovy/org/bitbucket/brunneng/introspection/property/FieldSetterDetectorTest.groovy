package org.bitbucket.brunneng.introspection.property

import spock.lang.Specification

class FieldSetterDetectorTest extends Specification {


  def 'test find and use fields setters'(List<Object> values) {
    when:
    def detector = new FieldSetterDetector()
    def a = new Bean1()
    List<Setter> setters = detector.detectAccessors(a.getClass(), new AccessorsDetectionOptions())
    def properties = ["i", "s"]
    then:
    setters.size() == properties.size()
    for (def fieldWithIndex : properties.withIndex()) {
      def fieldName = fieldWithIndex.first
      def index = fieldWithIndex.second
      setters.stream().filter { g -> g.propertyName == fieldName }.findFirst().get().set(a, values[index])
    }

    a.i == values.get(0)
    a.s == values.get(1)
    where:
    values  | _
    [5, "a"]     | _
    [17, "kdjf"] | _
    [-1, null]   | _
  }
}
