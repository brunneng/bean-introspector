package org.bitbucket.brunneng.introspection.property;

public class Bean1 {
   public static int staticField1;

   public static int getStaticField1() {
      return staticField1;
   }

   public static void setStaticField1(int staticField1) {
      Bean1.staticField1 = staticField1;
   }

   public int i;
   private String s;
   private final double d;

   public Bean1() {
      d = 0.0;
   }

   public Bean1(int i, String s, double d) {
      this.i = i;
      this.s = s;
      this.d = d;
   }

   public int getI() {
      return i;
   }

   public String getS() {
      return s;
   }

   public int get() {
      return 0;
   }

   public double getD() {
      return d;
   }

   public void setI(int i) {
      this.i = i;
   }

   public void setS(String s) {
      this.s = s;
   }
}
