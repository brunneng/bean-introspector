package org.bitbucket.brunneng.introspection.property

import spock.lang.Specification

class BeanIsBooleanGetterDetectorTest extends Specification {

  def 'test find and use bean is boolean getters'(List<Boolean> values) {
    when:
    def detector = new BeanIsBooleanGetterDetector()
    def a = new BeanWithBooleans(values[0], values[1])
    List<Getter> getters = detector.detectAccessors(a.getClass(), new AccessorsDetectionOptions())
    def fields = ["b1", "b2"]
    then:
    getters.size() == fields.size()
    for (def fieldWithIndex : fields.withIndex()) {
      def fieldName = fieldWithIndex.first
      def index = fieldWithIndex.second
      getters.stream().filter { g -> g.propertyName == fieldName }.findFirst().get().get(a) == values[index]
    }
    where:
    values         | _
    [true, true]   | _
    [false, false] | _
    [true, null]   | _
  }
}
