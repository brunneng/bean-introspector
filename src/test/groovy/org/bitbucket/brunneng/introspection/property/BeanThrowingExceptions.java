package org.bitbucket.brunneng.introspection.property;

public class BeanThrowingExceptions {
   void setValue12(String value) {
      throw new RuntimeException();
   }

   String getValue12() {
      throw new RuntimeException();
   }
}
