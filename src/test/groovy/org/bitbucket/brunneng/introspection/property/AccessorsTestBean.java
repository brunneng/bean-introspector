package org.bitbucket.brunneng.introspection.property;

public class AccessorsTestBean {
   private String s1;
   public String s2;
   public String s3;
   public String s4;
   private String s5;
   private String s6;

   public String getS3() {
      return s3;
   }
   public void setS3(String s3) {
      this.s3 = s3;
   }

   private String getS4() {
      return s4;
   }
   private void setS4(String s4) {
      this.s4 = s4;
   }

   public String getS5() {
      return s4;
   }
   public void setS5(String s5) {
      this.s5 = s5;
   }

   private String getS6() {
      return s4;
   }
   private void setS6(String s6) {
      this.s6 = s6;
   }
}
