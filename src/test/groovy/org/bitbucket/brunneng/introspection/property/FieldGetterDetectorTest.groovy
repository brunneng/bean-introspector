package org.bitbucket.brunneng.introspection.property

import spock.lang.Specification

class FieldGetterDetectorTest extends Specification {


  def 'test find and use fields getters'(List<Object> values) {
    when:
    def detector = new FieldGetterDetector()
    List<Getter> getters = detector.detectAccessors(Bean1.class, new AccessorsDetectionOptions())
    def a = new Bean1(values[0], values[1], values[2])
    def fields = ["i", "s", "d"]
    then:
    getters.size() == fields.size()
    for (def fieldWithIndex : fields.withIndex()) {
      def fieldName = fieldWithIndex.first
      def index = fieldWithIndex.second
      getters.stream().filter { g -> g.propertyName == fieldName }.findFirst().get().get(a) == values[index]
    }
    where:
    values       | _
    [1, "s", 1.0]     | _
    [17, "jdkf", 0.0] | _
    [-1, null, -1.0]   | _
  }
}
