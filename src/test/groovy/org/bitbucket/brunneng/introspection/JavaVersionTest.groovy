package org.bitbucket.brunneng.introspection


import spock.lang.Specification

class JavaVersionTest extends Specification {

  def 'java 17 is used for tests'() {
    expect:
    System.getProperty("java.version") startsWith "17"
  }

}
