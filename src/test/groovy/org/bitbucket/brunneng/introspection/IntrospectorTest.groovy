package org.bitbucket.brunneng.introspection

import org.bitbucket.brunneng.introspection.exceptions.PathResolvingException
import org.bitbucket.brunneng.introspection.property.TypeCategory
import spock.lang.Specification

import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

class IntrospectorTest extends Specification {

  def 'test is value type'(Class testClass, boolean matched) {
    when:
    def i = new Introspector()
    then:
    i.isValueType(testClass) == matched
    where:
    testClass           | matched
    Integer.TYPE        | true
    Integer.class       | true
    BigDecimal.class    | true
    Date.class          | true
    Calendar.class      | true
    LocalTime.class     | true
    LocalDate.class     | true
    LocalDateTime.class | true
    DayOfWeek.class     | true
    UUID.class          | true
    List.class          | false
    ArrayList.class     | false
    Collection.class    | false
    Map.class           | false
    new int[0].class    | false
    new int[0][0].class | false
    Bean1.class         | false
  }

  def 'test is collection type'(Class testClass, boolean matched) {
    when:
    def i = new Introspector()
    then:
    i.isCollectionType(testClass) == matched
    where:
    testClass           | matched
    List.class          | true
    ArrayList.class     | true
    Collection.class    | true
    Map.class           | false
    new int[0].class    | true
    new int[0][0].class | true
    Bean1.class         | false
    Integer.class       | false
  }

  def 'test is map type'(Class testClass, boolean matched) {
    when:
    def i = new Introspector()
    then:
    i.isMapType(testClass) == matched
    where:
    testClass     | matched
    List.class    | false
    Map.class     | true
    HashMap.class | true
  }

  def 'test is bean type'(Class testClass, boolean matched) {
    when:
    def i = new Introspector()
    then:
    i.isBeanType(testClass) == matched
    where:
    testClass           | matched
    List.class          | false
    new int[0].class    | false
    Map.class           | false
    Integer.class       | false
    BeanInterface.class | true
    Bean1.class         | true
  }

  def 'test add value type'() {
    when:
    def i = new Introspector()
    then:
    !i.isValueType(Bean1.class)
    when:
    i.addValueType(Bean1.class)
    then:
    i.isValueType(Bean1.class)
  }

  def 'test add collection type'() {
    when:
    def i = new Introspector()
    then:
    !i.isCollectionType(Bean1.class)
    when:
    i.addCollectionType(Bean1.class)
    then:
    i.isCollectionType(Bean1.class)
  }

  def 'test add map type'() {
    when:
    def i = new Introspector()
    then:
    !i.isMapType(Bean1.class)
    when:
    i.addMapType(Bean1.class)
    then:
    i.isMapType(Bean1.class)
  }

  def 'test get type category'(Class testClass, TypeCategory typeCategory) {
    when:
    def i = new Introspector()
    then:
    i.getTypeCategory(testClass) == typeCategory
    where:
    testClass           | typeCategory
    Integer.TYPE        | TypeCategory.Value
    Integer.class       | TypeCategory.Value
    BigDecimal.class    | TypeCategory.Value
    Date.class          | TypeCategory.Value
    Calendar.class      | TypeCategory.Value
    LocalTime.class     | TypeCategory.Value
    LocalDate.class     | TypeCategory.Value
    LocalDateTime.class | TypeCategory.Value
    DayOfWeek.class     | TypeCategory.Value
    List.class          | TypeCategory.Collection
    ArrayList.class     | TypeCategory.Collection
    Collection.class    | TypeCategory.Collection
    Map.class           | TypeCategory.Map
    new int[0].class    | TypeCategory.Collection
    new int[0][0].class | TypeCategory.Collection
    Bean1.class         | TypeCategory.Bean
  }

  def 'can get value by path'() {
    given:
    def bean3 = new Bean3("123")
    def bean2 = new Bean2(bean3)
    def i = new Introspector()
    when:
    def value = i.getValue(bean2, "bean3.value")
    then:
    value == "123"

    when:
    value = i.getValue(bean2, "bean3")
    then:
    value == bean3

    when:
    value = i.getValue(bean2, "bean3.value2")
    then:
    value == null
  }

  def 'can get owner of inner class if detectReferenceToParentOfNonStaticInnerClass == true'() {
    given:
    def bean3 = new Bean3("123")
    def i = new Introspector()
    i.accessorsDetectionOptions.detectReferenceToParentOfNonStaticInnerClass = true
    when:
    def value = i.getValue(bean3, 'this$0')
    then:
    value == this
  }

  def 'can\'t get owner of inner class'() {
    given:
    def bean3 = new Bean3("123")
    def i = new Introspector()
    when:
    i.getValue(bean3, 'this$0')
    then:
    PathResolvingException e = thrown()
    e.message == "Can't resolve 'this\$0': property 'Bean3.this\$0' is not found"
  }

  def 'can\'t get value by path because source object is null'() {
    given:
    def bean2 = new Bean2(null)
    def i = new Introspector()
    when:
    i.getValue(bean2, "bean3.value")
    then:
    PathResolvingException e = thrown()
    e.message == "Can't resolve 'value': source object is null"
  }

  def 'can\'t get value by path because source object not a bean'() {
    given:
    def bean3 = new Bean3("123")
    def bean2 = new Bean2(bean3)
    def i = new Introspector()
    when:
    i.getValue(bean2, "bean3.value.abc")
    then:
    PathResolvingException e = thrown()
    e.message == "Can't resolve 'abc': class java.lang.String is value type, not a bean"
  }

  def 'can\'t get value by path because source object has no requested property'() {
    given:
    def bean3 = new Bean3("123")
    def bean2 = new Bean2(bean3)
    def i = new Introspector()
    when:
    i.getValue(bean2, "bean3.test")
    then:
    PathResolvingException e = thrown()
    e.message == "Can't resolve 'test': property 'Bean3.test' is not found"
  }

  def 'can\'t get value by path because path is empty'() {
    given:
    def bean2 = new Bean2(null)
    def i = new Introspector()
    when:
    i.getValue(bean2, "")
    then:
    PathResolvingException e = thrown()
    e.message == "Can't resolve '': property 'Bean2.' is not found"
  }

  def 'can set value by path'() {
    given:
    def bean3 = new Bean3("123")
    def bean2 = new Bean2(bean3)
    def i = new Introspector()
    when:
    i.setValue(bean2, "bean3.value", "567")
    then:
    bean3.value == "567"

    when:
    def otherBean3 = new Bean3(null)
    i.setValue(bean2, "bean3", otherBean3)
    then:
    bean2.bean3 == otherBean3
  }

  def 'can\'t set value by path because source object is null'() {
    given:
    def bean2 = new Bean2(null)
    def i = new Introspector()
    when:
    i.setValue(bean2, "bean3.value", "567")
    then:
    PathResolvingException e = thrown()
    e.message == "Can't resolve 'value': source object is null"
  }

  def 'can\'t set value by path because source object not a bean'() {
    given:
    def bean3 = new Bean3("123")
    def bean2 = new Bean2(bean3)
    def i = new Introspector()
    when:
    i.setValue(bean2, "bean3.value.abc", 5)
    then:
    PathResolvingException e = thrown()
    e.message == "Can't resolve 'abc': class java.lang.String is value type, not a bean"
  }

  def 'can\'t set value by path because source object has no requested property'() {
    given:
    def bean3 = new Bean3("123")
    def bean2 = new Bean2(bean3)
    def i = new Introspector()
    when:
    i.setValue(bean2, "bean3.test", 5)
    then:
    PathResolvingException e = thrown()
    e.message == "Can't resolve 'test': property 'Bean3.test' is not found"
  }

  def 'can\'t set value by path because path is empty'() {
    given:
    def bean2 = new Bean2(null)
    def i = new Introspector()
    when:
    i.setValue(bean2, "", 5)
    then:
    PathResolvingException e = thrown()
    e.message == "Can't resolve '': property 'Bean2.' is not found"
  }

  def 'can set owner of inner class if detectReferenceToParentOfNonStaticInnerClass == true'() {
    given:
    def bean3 = new Bean3("123")
    def i = new Introspector()
    i.accessorsDetectionOptions.detectReferenceToParentOfNonStaticInnerClass = true
    def otherParent = new IntrospectorTest()
    when:
    i.setValue(bean3, 'this$0', otherParent)
    then:
    PathResolvingException e = thrown()
    e.message == "Can't resolve 'this\$0': no setter for property"
  }

  class Bean1 implements BeanInterface {

  }

  interface BeanInterface {

  }


  class Bean2 {
    Bean3 bean3

    Bean2(Bean3 bean3) {
      this.bean3 = bean3
    }
  }

  class Bean3 {
    String value
    String value2

    Bean3(String value) {
      this.value = value
    }
  }
}
