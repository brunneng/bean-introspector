package org.bitbucket.brunneng.introspection.util

import spock.lang.Specification

import java.time.DayOfWeek

class IsArrayClassMatcherTest extends Specification {

  def 'test is matched'(Class testClass, boolean matched) {
    when:
    def matcher = new IsArrayClassMatcher()
    then:
    matcher.isMatched(testClass) == matched
    where:
    testClass        | matched
    new int[0].class | true
    DayOfWeek.class  | false
    Date.class       | false
  }
}
