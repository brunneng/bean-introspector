package org.bitbucket.brunneng.introspection.util;

import java.util.List;

public class Generic1<T1 extends Number, T2 extends Comparable> {
   private T1 value1;
   private T2 value2;
   private List<T2> list;

   public T1 getValue1() {
      return value1;
   }

   public void setValue1(T1 value1) {
      this.value1 = value1;
   }

   public T2 getValue2() {
      return value2;
   }

   public void setValue2(T2 value2) {
      this.value2 = value2;
   }

   public List<T2> getList() {
      return list;
   }

   public void setList(List<T2> list) {
      this.list = list;
   }
}
