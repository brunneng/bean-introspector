package org.bitbucket.brunneng.introspection.util

import spock.lang.Specification

class IsAssignableClassMatcherTest extends Specification {

  def 'test is matched'(Class testClass, boolean matched) {
    when:
    def matcher = new IsAssignableClassMatcher(List.class)
    then:
    matcher.isMatched(testClass) == matched
    where:
    testClass        | matched
    List.class       | true
    ArrayList.class  | true
    Collection.class | false
    Map.class        | false
  }
}
