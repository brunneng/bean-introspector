package org.bitbucket.brunneng.introspection.util

import spock.lang.Specification

class PackageNameStartsWithClassMatcherTest extends Specification {

  def 'test is matched'(Class testClass, String packageName, boolean matched) {
    when:
    def matcher = new PackageNameStartsWithClassMatcher(packageName)
    then:
    matcher.isMatched(testClass) == matched
    where:
    testClass      | packageName     | matched
    List.class     | "java.util"     | true
    List.class     | "java.math"     | false
    Generic1.class | "org.bitbucket" | true
    Generic1.class | "org.bitBucket" | false
  }
}
