package org.bitbucket.brunneng.introspection.util

import org.bitbucket.brunneng.introspection.Introspector
import org.bitbucket.brunneng.introspection.util.p1.ClassA
import org.bitbucket.brunneng.introspection.util.p1.p2.ClassB
import spock.lang.Specification

import java.lang.reflect.*

class ReflectionUtilTest extends Specification {

  def 'test getAllDeclaredFields Generic1'() {
    when:
    List<Field> fields = ReflectionUtil.getAllDeclaredFields(Generic1.class)
    then:
    3 == fields.size()
    when:
    def value1Field = fields[0]
    def value2Field = fields[1]
    def listField = fields[2]
    then:
    value1Field.type == Number.class
    value1Field.genericType instanceof TypeVariable
    value1Field.genericType.typeName == "T1"
    value2Field.type == Comparable.class
    value2Field.genericType instanceof TypeVariable
    value2Field.genericType.typeName == "T2"
    listField.type == List.class
    listField.genericType.typeName == "java.util.List<T2>"
  }

  def 'test getAllDeclaredFields Child1Integer'() {
    when:
    List<Field> fields = ReflectionUtil.getAllDeclaredFields(Child1Integer.class)
    def typeVariableMap = ReflectionUtil.getActualTypeVariablesTypes(Child1Integer.class)
    then:
    3 == fields.size()
    when:
    def value1Field = fields[0]
    def value2Field = fields[1]
    def listField = fields[2]
    then:
    value1Field.type == Number.class
    value1Field.genericType instanceof TypeVariable
    value1Field.genericType.typeName == "T1"
    typeVariableMap.get(value1Field.genericType) == Integer.class
    value2Field.type == Comparable.class
    value2Field.genericType instanceof TypeVariable
    value2Field.genericType.typeName == "T2"
    typeVariableMap.get(value2Field.genericType) == String.class
    listField.type == List.class
    listField.genericType.typeName == "java.util.List<T2>"
  }

  def 'test getAllDeclaredMethods Generic1'() {
    when:
    List<Method> methods = ReflectionUtil.getAllDeclaredMethods(Generic1.class)
    then:
    6 == methods.size()
    when:
    def getValue1 = getMethodByName(methods, "getValue1")
    then:
    getValue1.returnType == Number.class
    getValue1.genericReturnType instanceof TypeVariable
    getValue1.genericReturnType.typeName == "T1"
    ReflectionUtil.getRawClass(getValue1.returnType) == getValue1.returnType
    when:
    def getValue2 = getMethodByName(methods, "getValue2")
    then:
    getValue2.returnType == Comparable.class
    getValue2.genericReturnType instanceof TypeVariable
    getValue2.genericReturnType.typeName == "T2"
    ReflectionUtil.getRawClass(getValue2.returnType) == getValue2.returnType
    when:
    ReflectionUtil.getRawClass(getValue1.genericReturnType)
    then:
    thrown IllegalArgumentException
    when:
    def getList = getMethodByName(methods, "getList")
    then:
    getList.returnType == List.class
    getList.genericReturnType instanceof ParameterizedType
    getList.genericReturnType.typeName == "java.util.List<T2>"
    ReflectionUtil.getRawClass(getList.genericReturnType) == List.class
  }

  def 'test getAllDeclaredMethods Child1Integer'() {
    when:
    List<Method> methods = ReflectionUtil.getAllDeclaredMethods(Child1Integer.class)
    def typeVariableMap = ReflectionUtil.getActualTypeVariablesTypes(Child1Integer.class)
    then:
    6 == methods.size()
    when:
    def getValue1 = getMethodByName(methods, "getValue1")
    then:
    getValue1.returnType == Number.class
    getValue1.genericReturnType instanceof TypeVariable
    getValue1.genericReturnType.typeName == "T1"
    typeVariableMap.get(getValue1.genericReturnType) == Integer.class
    when:
    def getValue2 = getMethodByName(methods, "getValue2")
    then:
    getValue2.returnType == Comparable.class
    getValue2.genericReturnType instanceof TypeVariable
    getValue2.genericReturnType.typeName == "T2"
    typeVariableMap.get(getValue2.genericReturnType) == String.class
  }

  def 'test getPackageName'(Class clazz, String packageName) {
    expect:
    ReflectionUtil.getPackageName(clazz) == packageName
    where:
    clazz                   | packageName
    Integer.class           | "java.lang"
    ReflectionUtil.class    | "org.bitbucket.brunneng.introspection.util"
    Class.forName("InRoot") | ""
  }

  def 'test getWrapperClassIfPrimitive'(Class inputClass, Class outputClass) {
    expect:
    ReflectionUtil.getWrapperClassIfPrimitive(inputClass) == outputClass
    where:
    inputClass     | outputClass
    Boolean.TYPE   | Boolean.class
    Character.TYPE | Character.class
    Integer.TYPE   | Integer.class
    Long.TYPE      | Long.class
    Float.TYPE     | Float.class
    Double.TYPE    | Double.class
    Short.TYPE     | Short.class
    Byte.TYPE      | Byte.class
    Date.class     | Date.class
    Integer.class  | Integer.class
  }

  def 'test findCommonType'(List values, Class commonType) {
    expect:
    ReflectionUtil.findCommonType(values) == commonType
    where:
    values             | commonType
    []                 | Object.class
    [1]                | Integer.class
    [1, 2.0]           | Number.class
    [1, "s"]           | Object.class
    [[1], [1].toSet()] | AbstractCollection.class
  }

  Method getMethodByName(List<Method> methods, String name) {
    return methods.stream().filter({ m -> m.name == name }).findFirst().get()
  }

  def 'test find classes in package'(String packageName, Set<Class> expectedClasses) {
    expect:
    ReflectionUtil.findClassesInPackage(packageName).toSet() == expectedClasses
    where:
    packageName                            | expectedClasses
    "org.bitbucket.brunneng.introspection.util.p1"    | [ClassA.class, ClassB.class]
    "org.bitbucket.brunneng.introspection.util.p1.p2" | [ClassB.class]
    "org.bitbucket.brunneng.introspection.util.wrong" | []
  }

  def 'test calcActualClass'() {
    when:
    def i = new Introspector()
    def actualTypeVariables = ReflectionUtil.getActualTypeVariablesTypes(Bean1.class)
    def childPropertyType = i.beanOfClass(Bean1.class)
            .property("child").propertyDescription.propertyAccessorValueType.valueType
    actualTypeVariables.putAll(ReflectionUtil.getActualTypeVariablesTypes(childPropertyType))
    def value1Type = i.beanOfClass(Bean2.class).property("value1")
            .getPropertyDescription().propertyAccessorValueType.valueType
    def value2Type = i.beanOfClass(Bean2.class).property("value2")
            .getPropertyDescription().propertyAccessorValueType.valueType
    then:
    ReflectionUtil.calcActualClass(value1Type, actualTypeVariables) == String.class
    ReflectionUtil.calcActualClass(value2Type, actualTypeVariables) == Long.class
  }

  def 'test calcActualClass is protected against cycles in actualTypeVariables map'() {
    given:
    def actualTypeVariables = new HashMap<TypeVariable<?>, Type>()
    def typeParameters = Bean2.class.typeParameters
    actualTypeVariables.put(typeParameters[0], typeParameters[1])
    actualTypeVariables.put(typeParameters[1], typeParameters[0])
    when:
    ReflectionUtil.calcActualClass(typeParameters[0], actualTypeVariables)
    then:
    def e = thrown IllegalArgumentException
    e.message.contains("Too many iterations")
  }

  def 'test getGenericParameterAtIndex'() {
    when:
    def i = new Introspector()
    def childPropertyType = i.beanOfClass(Bean1.class)
            .property("child").propertyDescription.propertyAccessorValueType.actualType
    then:
    ReflectionUtil.getGenericParameterAtIndex(childPropertyType, 0) == String.class
    ReflectionUtil.getGenericParameterAtIndex(childPropertyType, 1) == Long.class
  }


  def 'test can\'t ensureAccessible'() {
    when:
    def m = String.class.getDeclaredMethod("isLatin1")
    then:
    !ReflectionUtil.ensureAccessible(m)
  }

  def 'test can ensureAccessible'() {
    when:
    def f = Bean3.class.getDeclaredField("value")
    then:
    ReflectionUtil.ensureAccessible(f)
  }

  static class Bean1 extends AbstractBean<String> {

  }

  static class AbstractBean<T> {
    Bean2<T, Long> child
  }

  static class Bean2<T1, T2> {
    T1 value1
    T2 value2
  }

  static class Bean3 {
    private String value
  }
}
