package org.bitbucket.brunneng.introspection.util

import spock.lang.Specification

class IsPrimitiveClassMatcherTest extends Specification {

  def 'test is matched'(Class testClass, boolean matched) {
    when:
    def matcher = new IsPrimitiveClassMatcher()
    then:
    matcher.isMatched(testClass) == matched
    where:
    testClass        | matched
    new int[0].class | false
    Integer.TYPE     | true
    Integer.class    | false
  }
}
