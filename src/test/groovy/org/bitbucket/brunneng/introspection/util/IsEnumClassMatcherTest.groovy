package org.bitbucket.brunneng.introspection.util

import spock.lang.Specification

import java.time.DayOfWeek

class IsEnumClassMatcherTest extends Specification {

  def 'test is matched'(Class testClass, boolean matched) {
    when:
    def matcher = new IsEnumClassMatcher()
    then:
    matcher.isMatched(testClass) == matched
    where:
    testClass        | matched
    new int[0].class | false
    DayOfWeek.class  | true
    Date.class       | false
  }
}
