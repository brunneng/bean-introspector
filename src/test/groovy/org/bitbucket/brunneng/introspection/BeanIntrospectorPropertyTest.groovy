package org.bitbucket.brunneng.introspection


import org.bitbucket.brunneng.introspection.property.TypeCategory
import org.bitbucket.brunneng.introspection.util.ParameterizedTypeImpl
import spock.lang.Specification

import java.time.DayOfWeek

class BeanIntrospectorPropertyTest extends Specification {

  def 'test propertyDescription'() {
    when:
    def i = new Introspector()
    def bean = i.beanOfClass(Bean1.class)
    then:
    def v1PropertyDescription = bean.property("v1").propertyDescription
    v1PropertyDescription.ownerBeanClass == Bean1.class
    v1PropertyDescription.propertyName == "v1"
    v1PropertyDescription.propertyAccessorValueType.actualClass == String.class

    def v2AccessorType = bean.property("v2").propertyDescription.propertyAccessorValueType
    v2AccessorType.actualClass == List.class
    v2AccessorType.actualType instanceof ParameterizedTypeImpl
    v2AccessorType.typeCategory == TypeCategory.Collection
    v2AccessorType.getTypeOfGenericParameter(0) == Integer.class

    def v3AccessorType = bean.property("v3").propertyDescription.propertyAccessorValueType
    v3AccessorType.actualClass == Map.class
    v3AccessorType.actualType instanceof ParameterizedTypeImpl
    v3AccessorType.typeCategory == TypeCategory.Map
    v3AccessorType.getTypeOfGenericParameter(0) == String.class
    v3AccessorType.getTypeOfGenericParameter(1) == Long.class

    def v4AccessorType = bean.property("v4").propertyDescription.propertyAccessorValueType
    v4AccessorType.actualClass == List.class
    v4AccessorType.actualType instanceof ParameterizedTypeImpl
    v4AccessorType.typeCategory == TypeCategory.Collection
    v4AccessorType.getTypeOfGenericParameter(0) == String.class

    def v5AccessorType = bean.property("v5").propertyDescription.propertyAccessorValueType
    v5AccessorType.actualClass == List.class
    v5AccessorType.actualType instanceof ParameterizedTypeImpl
    v5AccessorType.typeCategory == TypeCategory.Collection
    v5AccessorType.getTypeOfGenericParameter(0) == Object.class

    def v6AccessorType = bean.property("v6").propertyDescription.propertyAccessorValueType
    v6AccessorType.actualClass == Map.class
    v6AccessorType.actualType instanceof ParameterizedTypeImpl
    v6AccessorType.typeCategory == TypeCategory.Map
    v6AccessorType.getTypeOfGenericParameter(0) == String.class
    v6AccessorType.getTypeOfGenericParameter(1) == Long.class

    def v7PropertyDescription = bean.property("parentV1").propertyDescription
    v7PropertyDescription.ownerBeanClass == Bean1.class
  }

  def 'test property description with recursion'() {
    when:
    def i = new Introspector()
    def bean = i.beanOfClass(Bean2.class)
    then:
    def d1 = bean.property("set").propertyDescription
    d1.propertyAccessorValueType.actualType instanceof ParameterizedTypeImpl
  }

  def 'test parent bean property'() {
    when:
    def i = new Introspector()
    def bean = i.beanOfClass(Bean1.class)
    then:
    bean.property("v1").parentBeanProperty == null
    bean.property("parentV1").parentBeanProperty == i.beanOfClass(Bean1Parent.class).property("parentV1")
  }

  class Bean1 extends Bean1Parent {
    String v1
    List<Integer> v2
    Map<String, Long> v3
    List<? extends String> v4
    List<? super String> v5
    Map<? extends String,? extends Long> v6
  }

  class Bean1Parent {
    String parentV1
  }

  static class GenericBean<T extends Enum<T>> {
    EnumSet<T> set
  }

  static class Bean2 extends GenericBean<DayOfWeek> {
  }
}
