package org.bitbucket.brunneng.introspection.package1;

/**
 * @author evvo
 */
public class SyntheticTestBean extends B {
}

class B {
    public int getSize() {
        return 0;
    }
}
