package org.bitbucket.brunneng.introspection

import org.bitbucket.brunneng.introspection.property.Getter
import org.bitbucket.brunneng.introspection.property.Setter

/**
 * Property of bean class. Can be accessed with [BeanIntrospector.property].
 *
 * @param name name of property
 * @param getters list of property getters
 * @param setters list of property setters
 */
open class Property(
    name: String, getters: List<Getter>, setters: List<Setter>,
    bean: BeanIntrospector
) : AbstractProperty<Property, BeanIntrospector>(name, getters, setters, bean)