package org.bitbucket.brunneng.introspection.exceptions

/**
 * Thrown on an attempt to get a property by name, but no such a property exists.
 */
class NoSuchPropertyException(message: String) : WrongBeanClassException(message)