package org.bitbucket.brunneng.introspection.exceptions

class PathResolvingException(message: String, cause: Exception?) : RuntimeException(message, cause) {
    constructor(message: String) : this(message, null)
}