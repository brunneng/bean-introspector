package org.bitbucket.brunneng.introspection.exceptions

class SetPropertyValueException(val property: String, val destBean: Any, cause: Exception) :
    RuntimeException("Failed to set property: $property", cause)