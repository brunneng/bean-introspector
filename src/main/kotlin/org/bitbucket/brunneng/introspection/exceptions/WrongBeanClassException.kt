package org.bitbucket.brunneng.introspection.exceptions

/**
 * Thrown on an attempt to introspect class as bean, while it's not actually a bean.
 */
open class WrongBeanClassException(message: String) : RuntimeException(message)