package org.bitbucket.brunneng.introspection.exceptions

class GetPropertyValueException(val property: String, val srcBean: Any, cause: Exception) :
    RuntimeException("Failed to get property: $property", cause)