package org.bitbucket.brunneng.introspection

import java.lang.reflect.Type

/**
 * Default implementation of [abstract introspector][AbstractIntrospector].
 */
open class Introspector : AbstractIntrospector<Property, BeanIntrospector>() {

    override fun createBean(beanType: Type): BeanIntrospector {
        return BeanIntrospector(beanType, this)
    }
}