package org.bitbucket.brunneng.introspection.util

/**
 * Class matcher which matches primitive classes
 */
class IsPrimitiveClassMatcher : ClassMatcher {

    override fun isMatched(clazz: Class<*>): Boolean {
        return clazz.isPrimitive
    }
}