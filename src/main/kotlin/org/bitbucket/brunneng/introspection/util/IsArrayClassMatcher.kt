package org.bitbucket.brunneng.introspection.util

/**
 * Class matcher which matches arrays
 */
class IsArrayClassMatcher : ClassMatcher {

    override fun isMatched(clazz: Class<*>): Boolean {
        return clazz.isArray
    }
}