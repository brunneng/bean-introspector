package org.bitbucket.brunneng.introspection.util

import java.io.File
import java.lang.reflect.*
import kotlin.collections.ArrayList
import kotlin.collections.Collection
import kotlin.collections.HashMap
import kotlin.collections.HashSet
import kotlin.collections.List
import kotlin.collections.Map
import kotlin.collections.MutableMap
import kotlin.collections.MutableSet
import kotlin.collections.addAll
import kotlin.collections.contains
import kotlin.collections.find
import kotlin.collections.flatMap
import kotlin.collections.isNotEmpty
import kotlin.collections.map
import kotlin.collections.set
import kotlin.collections.toList
import kotlin.collections.toTypedArray


object ReflectionUtil {

    /**
     * @param clazz class to search declared methods
     * @return all declared methods of class and all its superclasses, excluding ones from java.lang.Object
     */
    @JvmStatic
    fun getAllDeclaredMethods(clazz: Class<*>): List<Method> {
        val res = ArrayList<Method>()
        var currentClass: Class<*>? = clazz
        while (currentClass != null && currentClass != Object::class.java) {
            res.addAll(currentClass.declaredMethods)
            currentClass = currentClass.superclass
        }
        return res
    }

    /**
     * @param clazz class to search declared fields
     * @return all declared fields of class and all its superclasses
     */
    @JvmStatic
    fun getAllDeclaredFields(clazz: Class<*>): List<Field> {
        val res = ArrayList<Field>()
        var currentClass: Class<*>? = clazz
        while (currentClass != null) {
            res.addAll(currentClass.declaredFields)
            currentClass = currentClass.superclass
        }
        return res
    }

    @JvmStatic
    fun findDeclaredField(clazz: Class<*>, fieldName: String): Field? {
        return getAllDeclaredFields(clazz).find { f -> f.name == fieldName }
    }

    /**
     * Calculates mapping of type variables to actual classes. For example, if we have
     * class A<T> and class B extends A<Integer> then getActualTypeVariablesTypes(B.class) will return
     * map with entry: T -> Integer.
     * @param type type to search type variable mapping for it
     * @return map of type variables to actual classes
     */
    @JvmStatic
    fun getActualTypeVariablesTypes(type: Type): Map<TypeVariable<*>, Type> {
        val res = HashMap<TypeVariable<*>, Type>()

        var currentType: Type? = type
        while (currentType != null) {
            currentType = if (currentType is ParameterizedType) {
                extractFromParametrizedType(currentType, res)
                currentType.rawType
            } else if (currentType is Class<*>) {
                val genericSuperclass = currentType.genericSuperclass
                if (genericSuperclass is ParameterizedType) {
                    extractFromParametrizedType(genericSuperclass, res)
                }

                currentType.superclass
            } else {
                break
            }
        }

        return res
    }

    /**
     * @param type Class or ParameterizedType
     * @return raw class of given type
     */
    @JvmStatic
    fun getRawClass(type: Type): Class<*> {
        if (type is Class<*>) {
            return type
        } else if (type is ParameterizedType) {
            return type.rawType as Class<*>
        }

        throw IllegalArgumentException(
            "Type $type is unexpected! Should be either " +
                    "${Class::class.java.name} or ${ParameterizedType::class.java.name}"
        )
    }

    private fun extractFromParametrizedType(pt: ParameterizedType, res: MutableMap<TypeVariable<*>, Type>) {
        val rawType = pt.rawType
        if (rawType is Class<*>) {
            var i = 0
            for (tv in rawType.typeParameters) {
                res[tv] = pt.actualTypeArguments[i++]
            }
        }
    }

    /**
     * @param clazz class to get package of it
     *
     * @return package of given [clazz]
     */
    @JvmStatic
    fun getPackageName(clazz: Class<*>): String {
        val className = clazz.name
        val lastDot = className.lastIndexOf(".")
        var packageName = ""
        if (lastDot >= 0) {
            packageName = className.substring(0, lastDot)
        }
        return packageName
    }

    /**
     * @param type type to detect wrapper class of it
     *
     * @return if [type] is of primitive type returns is wrapper class, otherwise returns [type] itself
     */
    @JvmStatic
    fun getWrapperClassIfPrimitive(type: Type): Type {
        if ((type is Class<*> && !type.isPrimitive) || type == Void.TYPE || type is ParameterizedType) {
            return type
        }

        if (type == java.lang.Boolean.TYPE) {
            return java.lang.Boolean::class.java
        }
        if (type == java.lang.Double.TYPE) {
            return java.lang.Double::class.java
        }
        if (type == Integer.TYPE) {
            return java.lang.Integer::class.java
        }
        if (type == java.lang.Long.TYPE) {
            return java.lang.Long::class.java
        }
        if (type == Character.TYPE) {
            return java.lang.Character::class.java
        }
        if (type == java.lang.Float.TYPE) {
            return java.lang.Float::class.java
        }
        if (type == java.lang.Byte.TYPE) {
            return java.lang.Byte::class.java
        }
        if (type == java.lang.Short.TYPE) {
            return java.lang.Short::class.java
        }

        throw RuntimeException("class $type is not supported!")
    }

    /**
     * Makes given [accessibleObject] actually accessible, so that it can be used by reflection.
     *
     * @param accessibleObject constructor, field or method to make it accessible
     *
     * @return true if object is accessible, or it was forcibly made accessible
     */
    @JvmStatic
    fun ensureAccessible(accessibleObject: AccessibleObject): Boolean {
        if (accessibleObject is Member) {
            if (Modifier.isPublic(accessibleObject.modifiers)) {
                return true
            }
        }
        if (!accessibleObject.isAccessible) {
            try {
                accessibleObject.isAccessible = true
            } catch (e: java.lang.RuntimeException) {
                return false
            }
        }
        return true
    }

    /**
     * Finds common type for given [values], so every value has it as superclass. In worst case common class will be
     * [Object]. For example, if [values] contains Double 5.0 and Integer 1.0 then result value will be Number.
     *
     * @param values values to find their common type
     *
     * @return common type of given values
     */
    @JvmStatic
    fun findCommonType(values: Collection<Any?>): Class<*> {
        var res: Class<*>? = null
        for (v in values) {
            if (v == null) {
                continue
            }

            val valueClass = v::class.java
            if (res == null) {
                res = valueClass
            } else {
                while (res != null && res != Object::class.java && !res.isAssignableFrom(valueClass)) {
                    res = res.superclass
                }
            }
        }

        return res ?: Object::class.java
    }

    /**
     * Scans all classes accessible from the context class loader which belong to the given package and subpackages.
     *
     * @param packageName The base package
     * @return The classes
     */
    @JvmStatic
    fun findClassesInPackage(packageName: String): List<Class<*>> {
        val classLoader = Thread.currentThread().contextClassLoader!!
        val path = packageName.replace('.', '/')
        val dirs = classLoader.getResources(path).toList().map { File(it.file) }
        return dirs.flatMap { findClassesInPackage(it, packageName) }
    }

    /**
     * Recursive method which is used to find all classes in a given directory and subdirectories.
     *
     * @param directory   The base directory
     * @param packageName The package name for classes found inside the base directory
     * @return The classes
     */
    private fun findClassesInPackage(directory: File, packageName: String): List<Class<*>> {
        val classes = ArrayList<Class<*>>()
        if (!directory.exists()) {
            return classes
        }
        val files = directory.listFiles()
        if (files != null) {
            for (file in files) {
                val fileName = file.name
                if (file.isDirectory) {
                    if (!fileName.contains(".")) {
                        classes.addAll(findClassesInPackage(file, "$packageName.$fileName"))
                    }
                } else if (fileName.endsWith(".class")) {
                    classes.add(Class.forName("$packageName." + fileName.substring(0, fileName.length - 6)))
                }
            }
        }
        return classes
    }

    /**
     * Calculates actual class of given [genericType] by provided [actualTypeVariableTypes] map.
     *
     * For example, if map contains ((T1 -> T), (T -> Integer.class)) and genericType is T1, then
     * this method will return Integer.class.
     * @param genericType generic type.
     * @param actualTypeVariableTypes type variables map, which can be obtained by
     * [ReflectionUtil.getActualTypeVariablesTypes].
     */
    @JvmStatic
    fun calcActualClass(
        genericType: Type,
        actualTypeVariableTypes: Map<TypeVariable<*>, Type>
    ): Class<*> {
        return calcActualTypeInternal(genericType, actualTypeVariableTypes, false, HashSet()) as Class<*>
    }

    /**
     * Calculates actual type of given [genericType] by provided [actualTypeVariableTypes] map.
     *
     * For example, if map contains ((T1 -> T), (T -> Integer.class)) and genericType is T1, then
     * this method will return Integer.class.
     * @param genericType generic type.
     * @param actualTypeVariableTypes type variables map, which can be obtained by
     * [ReflectionUtil.getActualTypeVariablesTypes].
     */
    @JvmStatic
    fun calcActualType(genericType: Type, actualTypeVariableTypes: Map<TypeVariable<*>, Type>): Type {
        return calcActualTypeInternal(genericType, actualTypeVariableTypes, true, HashSet())
    }

    private fun calcActualTypeInternal(
        genericType: Type, actualTypeVariableTypes: Map<TypeVariable<*>, Type>,
        resolveParameterizedType: Boolean, visitedTypes: MutableSet<Type>
    ): Type {
        if (visitedTypes.contains(genericType)) {
            return genericType
        }
        visitedTypes.add(genericType)

        var currentType = genericType
        var iterations = 0

        while (true) {
            if (++iterations > 100) {
                throw IllegalArgumentException("Too many iterations. Probably actualTypeVariableTypes " +
                        "map $actualTypeVariableTypes has cycles")
            }

            if (currentType is Class<*>) {
                return currentType
            }

            if (currentType is TypeVariable<*>) {
                if (actualTypeVariableTypes.contains(currentType)) {
                    currentType = actualTypeVariableTypes[currentType]!!
                    continue
                }
                val bounds = currentType.bounds
                if (bounds.isNotEmpty()) {
                    currentType = bounds[0]
                    continue
                }
            }

            if (currentType is ParameterizedType) {
                return if (resolveParameterizedType) {
                    val resolvedTypeArguments = currentType.actualTypeArguments.map {
                        calcActualTypeInternal(it, actualTypeVariableTypes, true, visitedTypes)
                    }
                        .toTypedArray()
                    val ownerType = currentType.ownerType ?: Any::class.java
                    ParameterizedTypeImpl(ownerType, currentType.rawType, resolvedTypeArguments)
                } else {
                    currentType.rawType
                }
            }

            if (currentType is GenericArrayType) {
                val componentType = calcActualClass(currentType.genericComponentType, actualTypeVariableTypes)
                return java.lang.reflect.Array.newInstance(componentType, 0).javaClass
            }

            if (currentType is WildcardType) {
                return currentType.upperBounds[0]
            }

            break
        }

        return Object::class.java
    }

    /**
     * @return if [valueType] is ParameterizedType then returns type argument at position [parameterIndex], otherwise
     * returns null.
     */
    @JvmStatic
    fun getGenericParameterAtIndex(
        valueType: Type,
        parameterIndex: Int
    ): Type? {
        if (valueType is ParameterizedType) {
            val typeArguments = valueType.actualTypeArguments
            if (typeArguments.size > parameterIndex) {
                return typeArguments[parameterIndex]
            }
        }
        return null
    }
}
