package org.bitbucket.brunneng.introspection.util

/**
 * Class matcher which matches only classes which are assignable to given [parentClass]
 *
 * @param parentClass class from which all matched classes are inherited
 */
class IsAssignableClassMatcher(private val parentClass: Class<*>) : ClassMatcher {

    override fun isMatched(clazz: Class<*>): Boolean {
        return parentClass.isAssignableFrom(clazz)
    }
}