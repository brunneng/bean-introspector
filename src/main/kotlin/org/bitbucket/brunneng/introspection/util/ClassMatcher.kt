package org.bitbucket.brunneng.introspection.util

/**
 * Matcher which tests that the class matches certain conditions.
 */
interface ClassMatcher {
    fun isMatched(clazz: Class<*>): Boolean
}