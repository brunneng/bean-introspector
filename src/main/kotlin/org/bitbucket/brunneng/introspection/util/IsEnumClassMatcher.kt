package org.bitbucket.brunneng.introspection.util

/**
 * Class matcher which matches enums
 */
class IsEnumClassMatcher : ClassMatcher {

    override fun isMatched(clazz: Class<*>): Boolean {
        return clazz.isEnum
    }
}