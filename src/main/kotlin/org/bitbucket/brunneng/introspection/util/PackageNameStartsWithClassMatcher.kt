package org.bitbucket.brunneng.introspection.util

/**
 * Matcher which matches classes whose package starts from given [packageStart]
 *
 * @param packageStart package start of matched classes
 */
class PackageNameStartsWithClassMatcher(private val packageStart: String) : ClassMatcher {
    override fun isMatched(clazz: Class<*>): Boolean {
        return ReflectionUtil.getPackageName(clazz).startsWith(packageStart)
    }
}