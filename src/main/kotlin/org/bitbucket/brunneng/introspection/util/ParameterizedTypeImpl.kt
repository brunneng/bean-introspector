package org.bitbucket.brunneng.introspection.util

import org.bitbucket.brunneng.introspection.property.AccessorValueType
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

/**
 * Implementation of [ParameterizedType] to hold resolved generic types in [AccessorValueType.actualType]
 */
class ParameterizedTypeImpl(
    private val ownerType: Type, private val rawType: Type,
    private val actualTypeArguments: Array<Type>
) : ParameterizedType {

    override fun getActualTypeArguments(): Array<Type> {
        return actualTypeArguments
    }

    override fun getRawType(): Type {
        return rawType
    }

    override fun getOwnerType(): Type {
        return ownerType
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ParameterizedTypeImpl

        if (ownerType != other.ownerType) return false
        if (rawType != other.rawType) return false
        if (!actualTypeArguments.contentEquals(other.actualTypeArguments)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = ownerType.hashCode()
        result = 31 * result + rawType.hashCode()
        result = 31 * result + actualTypeArguments.contentHashCode()
        return result
    }

    override fun toString(): String {
        val arguments = actualTypeArguments.joinToString(",")
        return "$rawType<$arguments>"
    }


}