package org.bitbucket.brunneng.introspection.property

import org.bitbucket.brunneng.introspection.exceptions.SetPropertyValueException
import java.lang.reflect.Member

/**
 * Abstract setter [accessor][Accessor]. Used to set property value to destination model.
 */
abstract class Setter(propertyName: String, accessorValueType: AccessorValueType, fieldOrMethod: Member) :
    Accessor(propertyName, accessorValueType, fieldOrMethod) {

    /**
     * Sets value to property of [destBean]
     * @param destBean destination bean to set property value.
     * @param value value to be set
     */
    fun set(destBean: Any, value: Any?) {
        try {
            ensureAccessible()
            setInternal(destBean, value)
        } catch (e: Exception) {
            throw SetPropertyValueException(propertyName, destBean, e)
        }
    }

    @Throws(Throwable::class)
    protected abstract fun setInternal(destBean: Any, value: Any?)
}
