package org.bitbucket.brunneng.introspection.property

import java.lang.reflect.Method

/**
 * Abstract detector of property accessors which are represented as non-static methods with some [accessorPrefix].
 * @param accessorPrefix accessor prefix, it can be for example: "get", "set", "is" etc
 */
abstract class AbstractBeanMethodAccessorDetector<T : Accessor>(private val accessorPrefix: String) :
    AbstractMethodAccessorDetector<T>() {

    override fun isAccessorMethod(method: Method): Boolean {
        val methodName = method.name
        return super.isAccessorMethod(method) &&
                methodName.startsWith(accessorPrefix) && methodName.length > accessorPrefix.length &&
                methodName[accessorPrefix.length].isUpperCase()
    }

    override fun getPropertyName(methodName: String): String {
        var propertyName = methodName.substring(accessorPrefix.length)
        if (propertyName.length > 1) {
            if (!propertyName[1].isUpperCase()) {
                propertyName = propertyName[0].toLowerCase() + propertyName.substring(1)
            }
        } else {
            propertyName = propertyName.toLowerCase()
        }

        return propertyName
    }
}