package org.bitbucket.brunneng.introspection.property

import java.lang.reflect.Field

class FieldGetter(propertyName: String, accessorValueType: AccessorValueType, field: Field) :
    Getter(propertyName, accessorValueType, field) {

    override fun getInternal(srcBean: Any): Any? {
        return (fieldOrMethod as Field).get(srcBean)
    }

}