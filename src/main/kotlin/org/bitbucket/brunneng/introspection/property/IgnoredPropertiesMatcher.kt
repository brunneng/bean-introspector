package org.bitbucket.brunneng.introspection.property

/**
 * Detects properties which should be completely ignored by introspector. Can be used to filter out some system
 * properties, like **metaClass** in groovy.
 */
interface IgnoredPropertiesMatcher {

    /**
     * @param propertyDescription description of analyzed property
     *
     * @return Is should this property be ignored by introspector? if 'true' then effect will be like this property
     * does not exist
     */
    fun isShouldIgnoreProperty(propertyDescription: PropertyDescription): Boolean
}