package org.bitbucket.brunneng.introspection.property

import org.bitbucket.brunneng.introspection.util.ReflectionUtil
import java.lang.invoke.MethodHandle
import java.lang.invoke.MethodHandles
import java.lang.invoke.MethodType
import java.lang.reflect.*

/**
 * Abstract accessor, which is used to get or set values in beans.
 * Can be [Getter] or [Setter].
 *
 * @param propertyName name of property which corresponds to this accessor.
 * @param accessorValueType type information about value of this accessor
 * @param fieldOrMethod member of class, corresponding to accessor. It can be field or method. Used to get or
 * set values by reflection.
 *
 * @see AccessorValueType
 */
abstract class Accessor(val propertyName: String, val accessorValueType: AccessorValueType, val fieldOrMethod: Member) {

    private var methodHandle: MethodHandle? = null
    private var accessible: Boolean? = null

    /**
     * @return is [fieldOrMethod] public?
     */
    fun isPublic(): Boolean {
        return Modifier.isPublic(fieldOrMethod.modifiers)
    }

    /**
     * @return is [fieldOrMethod]'s declaring class public?
     */
    fun isPublicOwner(): Boolean {
        return Modifier.isPublic(fieldOrMethod.declaringClass.modifiers)
    }

    /**
     * @return is [fieldOrMethod] synthetic?
     */
    fun isSynthetic(): Boolean {
        return fieldOrMethod.isSynthetic
    }

    /**
     * @return declared annotations on field or method corresponding to this accessor
     */
    fun getAnnotations(): List<Annotation> {
        val declaredAnnotations: Array<out Annotation>? = if (fieldOrMethod is Field) {
            fieldOrMethod.declaredAnnotations
        } else {
            (fieldOrMethod as Method).declaredAnnotations
        }
        if (declaredAnnotations != null) {
            return declaredAnnotations.toList()
        }
        return emptyList()
    }

    protected fun getMethodHandle(): MethodHandle {
        val res = methodHandle
        if (res != null) {
            return res
        }

        val lookup = MethodHandles.lookup()
        methodHandle = lookup.unreflect(fieldOrMethod as Method).asFixedArity().asType(getGenericMethodHandleType())
        return methodHandle!!
    }

    /**
     * Makes given underlying method or field is accessible, so that it can be used by reflection.
     * Does nothing, if it's already accessible.
     *
     * @return true if underlying method or field is accessible, or it was forcibly made accessible
     */
    fun ensureAccessible(): Boolean {
        if (accessible != null) {
            return accessible!!;
        }

        if (!isPublic() && fieldOrMethod is AccessibleObject) {
            accessible = ReflectionUtil.ensureAccessible(fieldOrMethod)
        }
        accessible = true
        return true;
    }

    protected open fun getGenericMethodHandleType(): MethodType {
        return MethodType.methodType(Any::class.java, Any::class.java)
    }
}
