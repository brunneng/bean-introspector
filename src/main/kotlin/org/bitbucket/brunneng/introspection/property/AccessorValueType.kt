package org.bitbucket.brunneng.introspection.property

import org.bitbucket.brunneng.introspection.util.ReflectionUtil
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.lang.reflect.TypeVariable

/**
 * Represents detailed information about value type of accessor including knowledge about generic parameters.
 *
 * @param valueType type of accessor, can be either [java.lang.reflect.ParameterizedType] or [Class].
 * @param actualTypeVariableTypes map of genetic type variables to actual types
 */
class AccessorValueType(
    val valueType: Type,
    val actualTypeVariableTypes: Map<TypeVariable<*>, Type>
) {

    /**
     * Class of accessor value with resolved generic type
     */
    val actualClass: Class<*> = ReflectionUtil.getWrapperClassIfPrimitive(
        ReflectionUtil.calcActualClass(valueType, actualTypeVariableTypes)
    ) as Class<*>

    /**
     * Type of accessor value with resolved generic type
     */
    val actualType = ReflectionUtil.getWrapperClassIfPrimitive(
        ReflectionUtil.calcActualType(valueType, actualTypeVariableTypes)
    )

    /**
     * [Type category][TypeCategory] of accessor. By default it's [TypeCategory.Undefined] but during building
     * configuration of bean it's clarified.
     */
    var typeCategory: TypeCategory = TypeCategory.Undefined

    /**
     * @return type of generic parameter of given index. For example if type of accessor is List < String > , then
     * getTypeOfGenericParameter(0) will return String.
     */
    fun getTypeOfGenericParameter(parameterIndex: Int): Type? {
        if (actualType is ParameterizedType) {
            val typeArguments = actualType.actualTypeArguments
            if (typeArguments.size > parameterIndex) {
                return typeArguments[parameterIndex]
            }
        }
        return null
    }

}