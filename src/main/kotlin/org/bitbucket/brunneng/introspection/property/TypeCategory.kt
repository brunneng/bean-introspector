package org.bitbucket.brunneng.introspection.property

import org.bitbucket.brunneng.introspection.AbstractBeanIntrospector
import org.bitbucket.brunneng.introspection.AbstractIntrospector

/**
 * Category of property type.
 */
enum class TypeCategory {
    /**
     * Value type
     *
     * @see AbstractIntrospector.isValueType
     */
    Value,

    /**
     * Collection type
     *
     * @see AbstractIntrospector.isCollectionType
     */
    Collection,

    /**
     * Map type
     *
     * @see AbstractIntrospector.isMapType
     */
    Map,

    /**
     * Bean type
     *
     * @see AbstractBeanIntrospector
     */
    Bean,

    /**
     * Represents category, which is not detected yet by introspector
     */
    Undefined
}