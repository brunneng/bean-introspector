package org.bitbucket.brunneng.introspection.property

import java.lang.reflect.Method
import java.lang.reflect.Type
import java.lang.reflect.TypeVariable

/**
 * Detector of bean property getters which are represented as non-static and not synthetic methods with prefix
 * "is" and returns [Boolean]. Value of property will be resolved using reflection. Private methods are also detected.
 */
class BeanIsBooleanGetterDetector : AbstractBeanMethodAccessorDetector<MethodGetter>("is") {

    override fun isAccessorMethod(method: Method): Boolean {
        return super.isAccessorMethod(method) &&
                (method.returnType == Boolean::class.java || method.returnType == java.lang.Boolean::class.java)
                && method.parameterCount == 0
    }

    override fun createAccessor(
        propertyName: String, method: Method,
        actualTypeVariableTypes: Map<TypeVariable<*>, Type>
    ): MethodGetter {

        val typeInfo = AccessorValueType(method.genericReturnType, actualTypeVariableTypes)
        return MethodGetter(propertyName, typeInfo, method)
    }
}