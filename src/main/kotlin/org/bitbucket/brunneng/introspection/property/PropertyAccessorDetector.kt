package org.bitbucket.brunneng.introspection.property

import java.lang.reflect.Type

/**
 * Abstraction to search [accessors][Accessor] within class.
 */
interface PropertyAccessorDetector<T : Accessor> {

    /**
     * @param objectType type of object, for which accessors should be detected.
     * Can be either [java.lang.reflect.ParameterizedType] or [Class].
     * @param options options of accessors detection
     *
     * @return list of detected accessors
     */
    fun detectAccessors(objectType: Type, options: AccessorsDetectionOptions): List<T>
}