package org.bitbucket.brunneng.introspection.property

import java.lang.reflect.Field
import java.lang.reflect.Modifier
import java.lang.reflect.Type
import java.lang.reflect.TypeVariable

/**
 * Detector of setters which are represented as non-static and not synthetic fields. Value of field will be set
 * using reflection. Private fields are also detected.
 */
class FieldSetterDetector : AbstractFieldAccessorDetector<FieldSetter>() {

    override fun isAccessorMethod(field: Field, options: AccessorsDetectionOptions): Boolean {
        return super.isAccessorMethod(field, options) && !Modifier.isFinal(field.modifiers)
    }

    override fun createAccessor(field: Field, actualTypeVariableTypes: Map<TypeVariable<*>, Type>): FieldSetter {

        val typeInfo = AccessorValueType(field.genericType, actualTypeVariableTypes)
        return FieldSetter(field.name, typeInfo, field)
    }
}