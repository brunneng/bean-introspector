package org.bitbucket.brunneng.introspection.property;

import java.lang.invoke.MethodType;
import java.lang.reflect.Member;

public class MethodSetter extends Setter {

    public MethodSetter(String propertyName, AccessorValueType accessorValueType, Member fieldOrMethod) {
        super(propertyName, accessorValueType, fieldOrMethod);
    }

    @Override
    protected void setInternal(Object destBean, Object value) throws Throwable {
        getMethodHandle().invokeExact(destBean, value);
    }

    @Override
    protected MethodType getGenericMethodHandleType() {
        return MethodType.methodType(void.class, Object.class, Object.class);
    }
}
