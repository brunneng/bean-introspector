package org.bitbucket.brunneng.introspection.property

/**
 * Options to control nuances of assessors detection
 */
class AccessorsDetectionOptions(var detectReferenceToParentOfNonStaticInnerClass: Boolean = false)