package org.bitbucket.brunneng.introspection.property

/**
 * Detects auxiliary properties which are added to classes defined in groovy and should be excluded from introspection.
 */
class GroovyIgnoredPropertiesMatcher : IgnoredPropertiesMatcher {
    override fun isShouldIgnoreProperty(propertyDescription: PropertyDescription): Boolean {
        if (propertyDescription.propertyName == "metaClass"
            && propertyDescription.propertyAccessorValueType.actualClass.name == "groovy.lang.MetaClass"
        ) {
            return true
        }

        return false
    }
}