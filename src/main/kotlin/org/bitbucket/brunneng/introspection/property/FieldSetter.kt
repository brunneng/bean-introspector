package org.bitbucket.brunneng.introspection.property

import java.lang.reflect.Field

class FieldSetter(propertyName: String, accessorValueType: AccessorValueType, field: Field) :
    Setter(propertyName, accessorValueType, field) {

    override fun setInternal(destBean: Any, value: Any?) {
        (fieldOrMethod as Field).set(destBean, value)
    }
}