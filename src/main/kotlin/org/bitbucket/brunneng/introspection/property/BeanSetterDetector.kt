package org.bitbucket.brunneng.introspection.property

import java.lang.reflect.Method
import java.lang.reflect.Type
import java.lang.reflect.TypeVariable

/**
 * Detector of bean property setters which are represented as non-static and not synthetic methods with prefix "set".
 * Value of property will be set using reflection. Private methods are also detected.
 */
class BeanSetterDetector : AbstractBeanMethodAccessorDetector<MethodSetter>("set") {

    override fun isAccessorMethod(method: Method): Boolean {
        return super.isAccessorMethod(method) && method.returnType == Void.TYPE && method.parameterCount == 1
    }

    override fun createAccessor(
        propertyName: String, method: Method,
        actualTypeVariableTypes: Map<TypeVariable<*>, Type>
    ): MethodSetter {

        val parameter = method.parameters[0]
        val typeInfo = AccessorValueType(parameter.parameterizedType, actualTypeVariableTypes)
        return MethodSetter(propertyName, typeInfo, method)

    }
}