package org.bitbucket.brunneng.introspection.property;

import java.lang.invoke.MethodType;
import java.lang.reflect.Member;

public class MethodGetter extends Getter {

    public MethodGetter(String propertyName, AccessorValueType accessorValueType, Member fieldOrMethod) {
        super(propertyName, accessorValueType, fieldOrMethod);
    }

    @Override
    protected Object getInternal(Object srcBean) throws Throwable {
        return getMethodHandle().invokeExact(srcBean);
    }

    @Override
    public MethodType getGenericMethodHandleType() {
        return MethodType.methodType(Object.class, Object.class);
    }
}
