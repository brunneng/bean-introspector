package org.bitbucket.brunneng.introspection.property

import org.bitbucket.brunneng.introspection.exceptions.GetPropertyValueException
import java.lang.reflect.Member

/**
 * Abstract getter [accessor][Accessor]. Used to get property value from source model.
 */
abstract class Getter(propertyName: String, accessorValueType: AccessorValueType, fieldOrMethod: Member) :
    Accessor(propertyName, accessorValueType, fieldOrMethod) {

    /**
     * [srcBean] source bean to take value of property from it.
     * @return property value of [srcBean]
     */
    fun get(srcBean: Any): Any? {
        try {
            ensureAccessible()
            return getInternal(srcBean)
        } catch (e: Exception) {
            throw GetPropertyValueException(propertyName, srcBean, e)
        }
    }

    @Throws(Throwable::class)
    protected abstract fun getInternal(srcBean: Any): Any?
}
