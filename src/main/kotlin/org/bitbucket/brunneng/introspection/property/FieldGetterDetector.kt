package org.bitbucket.brunneng.introspection.property

import java.lang.reflect.Field
import java.lang.reflect.Type
import java.lang.reflect.TypeVariable

/**
 * Detector of getters which are represented as non-static and not synthetic fields. Value of field will be resolved
 * using reflection. Private fields are also detected.
 */
class FieldGetterDetector : AbstractFieldAccessorDetector<FieldGetter>() {

    override fun createAccessor(field: Field, actualTypeVariableTypes: Map<TypeVariable<*>, Type>): FieldGetter {

        val typeInfo = AccessorValueType(field.genericType, actualTypeVariableTypes)
        return FieldGetter(field.name, typeInfo, field)
    }
}