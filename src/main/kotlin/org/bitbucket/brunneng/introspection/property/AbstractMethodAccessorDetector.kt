package org.bitbucket.brunneng.introspection.property

import org.bitbucket.brunneng.introspection.util.ReflectionUtil
import java.lang.reflect.Method
import java.lang.reflect.Modifier
import java.lang.reflect.Type
import java.lang.reflect.TypeVariable
import java.util.stream.Collectors

/**
 * Abstract detector of property accessors which are represented as non-static and not synthetic methods.
 */
abstract class AbstractMethodAccessorDetector<T : Accessor> : PropertyAccessorDetector<T> {

    override fun detectAccessors(objectType: Type, options: AccessorsDetectionOptions): List<T> {
        val methods = ReflectionUtil.getAllDeclaredMethods(ReflectionUtil.getRawClass(objectType))
        val actualTypeVariableTypes = ReflectionUtil.getActualTypeVariablesTypes(objectType)

        return methods.stream().filter { m -> isAccessorMethod(m) }
            .map { m -> createAccessor(getPropertyName(m.name), m, actualTypeVariableTypes) }
            .collect(Collectors.toList())
    }

    protected open fun isAccessorMethod(method: Method): Boolean {
        return !Modifier.isStatic(method.modifiers)
    }

    protected abstract fun getPropertyName(methodName: String): String

    protected abstract fun createAccessor(
        propertyName: String, method: Method,
        actualTypeVariableTypes: Map<TypeVariable<*>, Type>
    ): T
}