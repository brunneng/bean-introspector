package org.bitbucket.brunneng.introspection.property

import org.bitbucket.brunneng.introspection.util.ReflectionUtil
import java.lang.reflect.Field
import java.lang.reflect.Modifier
import java.lang.reflect.Type
import java.lang.reflect.TypeVariable
import java.util.stream.Collectors

const val REFERENCE_TO_PARENT_OF_NON_STATIC_INNER_CLASS = "this$0"

/**
 * Abstract detector of property accessors which are represented as non-static and not synthetic fields.
 */
abstract class AbstractFieldAccessorDetector<T : Accessor> : PropertyAccessorDetector<T> {

    override fun detectAccessors(
        objectType: Type,
        options: AccessorsDetectionOptions
    ): List<T> {
        val fields = ReflectionUtil.getAllDeclaredFields(ReflectionUtil.getRawClass(objectType))
        val actualTypeVariableTypes = ReflectionUtil.getActualTypeVariablesTypes(objectType)

        return fields.stream()
            .filter { f -> isAccessorMethod(f, options) }
            .map { f -> createAccessor(f, actualTypeVariableTypes) }.collect(Collectors.toList())
    }

    protected open fun isAccessorMethod(field: Field, options: AccessorsDetectionOptions): Boolean {
        return !Modifier.isStatic(field.modifiers) && (!field.isSynthetic
                || (options.detectReferenceToParentOfNonStaticInnerClass
                && field.name.equals(REFERENCE_TO_PARENT_OF_NON_STATIC_INNER_CLASS)))
    }

    protected abstract fun createAccessor(field: Field, actualTypeVariableTypes: Map<TypeVariable<*>, Type>): T
}