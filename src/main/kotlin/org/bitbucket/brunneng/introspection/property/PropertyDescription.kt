package org.bitbucket.brunneng.introspection.property

/**
 * Represents description of property. Used in [IgnoredPropertiesMatcher]
 *
 * @param ownerBeanClass bean class which contains this property (itself or any of superclasses)
 * @param propertyName name of property
 * @param propertyAccessorValueType value type of accessor which can give information about generic parameters.
 */
class PropertyDescription(
    val ownerBeanClass: Class<*>, val propertyName: String,
    val propertyAccessorValueType: AccessorValueType
) {

    override fun toString(): String {
        return "${ownerBeanClass.simpleName}.$propertyName: ${propertyAccessorValueType.actualType}"
    }
}