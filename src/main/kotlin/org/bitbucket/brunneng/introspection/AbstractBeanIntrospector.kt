package org.bitbucket.brunneng.introspection

import org.bitbucket.brunneng.introspection.exceptions.NoSuchPropertyException
import org.bitbucket.brunneng.introspection.exceptions.WrongBeanClassException
import org.bitbucket.brunneng.introspection.property.Getter
import org.bitbucket.brunneng.introspection.property.Setter
import org.bitbucket.brunneng.introspection.util.ReflectionUtil
import java.lang.reflect.Type
import java.util.*
import java.util.stream.Collectors

/**
 * Abstract introspector for bean and it's properties.
 *
 * Extend from this class to add custom metadata to your bean class definitions.
 *
 * Contains a [reference to parent bean introspector][parentBeanIntrospector], which corresponds to a bean class from
 * which it extends. This can be handy to support inheritance of metadata in your custom implementations.
 *
 * @param beanType the type of bean
 * @param introspector the introspector, which have created this instance
 * @param P class of introspected property
 * @param B class of bean introspector
 *
 * @see AbstractProperty
 */
abstract class AbstractBeanIntrospector<P : AbstractProperty<P, B>, B : AbstractBeanIntrospector<P, B>>
protected constructor(val beanType: Type, internal val introspector: AbstractIntrospector<P, B>) {

    val beanClass: Class<*> = ReflectionUtil.getRawClass(beanType)
    val parentBeanIntrospector: B?

    private val propertiesByName = TreeMap<String, P>()

    init {
        if (beanClass == Object::class.java) {
            throw WrongBeanClassException("java.lang.Object is not a bean")
        }
        if (introspector.isValueType(beanClass)) {
            throw WrongBeanClassException("class ${beanClass.name} is value type, not a bean")
        }
        if (introspector.isCollectionType(beanClass)) {
            throw WrongBeanClassException("class ${beanClass.name} is collection type, not a bean")
        }
        if (introspector.isMapType(beanClass)) {
            throw WrongBeanClassException("class ${beanClass.name} is map type, not a bean")
        }

        parentBeanIntrospector = resolveParentBeanIntrospector()

        val gettersByPropertyName = introspector.getterDetectors.stream()
            .flatMap { gd -> gd.detectAccessors(beanType, introspector.accessorsDetectionOptions).stream() }
            .collect(Collectors.groupingBy<Getter, String> { g -> g.propertyName })

        val settersByPropertyName = introspector.setterDetectors.stream()
            .flatMap { sd -> sd.detectAccessors(beanType, introspector.accessorsDetectionOptions).stream() }
            .collect(Collectors.groupingBy<Setter, String> { s -> s.propertyName })

        val allPropertyNames = gettersByPropertyName.keys.union(settersByPropertyName.keys)
        for (propertyName in allPropertyNames) {
            val getters = gettersByPropertyName.getOrElse(propertyName) { emptyList() }
            val setters = settersByPropertyName.getOrElse(propertyName) { emptyList() }
            val property = createProperty(propertyName, getters, setters)

            if (introspector.ignoredPropertiesMatchers.stream()
                    .noneMatch { m -> m.isShouldIgnoreProperty(property.getPropertyDescription()) }
            ) {
                propertiesByName[propertyName] = property
            }
        }
    }

    private fun resolveParentBeanIntrospector() = if (beanType != beanClass) {
        introspector.beanOfClass(beanClass)
    } else {
        val superclass = beanClass.superclass
        if (superclass != Object::class.java && superclass != null) {
            introspector.beanOfClass(superclass)
        } else {
            null
        }
    }

    /**
     * Collects all annotations from bean and from parent beans. The collection could contain instance of
     * the same annotation if it has different parameters.
     * @return ordered collection of all annotation instances without duplications.
     */
    fun getAnnotations(): LinkedHashSet<Annotation> {
        val res = LinkedHashSet<Annotation>()
        val annotations = beanClass.annotations
        if (annotations != null) {
            res.addAll(annotations)
        }
        if (parentBeanIntrospector != null) {
            res.addAll(parentBeanIntrospector.getAnnotations())
        }
        return res
    }

    /**
     * @param name the name of property
     * @param getters the getters of property
     * @param setters the setters of property
     * @return new created property for given name and accessors
     */
    protected abstract fun createProperty(name: String, getters: List<Getter>, setters: List<Setter>): P

    /**
     * @param name name of property in bean
     *
     * @return [introspected property][P] with given [name].
     * It will be created, if doesn't exist yet, and cached.
     * So subsequent similar calls to this method will return the same instance.
     *
     * @throws NoSuchPropertyException if no such property exists in bean
     */
    fun property(name: String): P {
        return propertiesByName[name] ?: throw NoSuchPropertyException(
            "property '${beanClass.simpleName}.${name}' is not found"
        )
    }

    /**
     * @param name name of property
     *
     * @return is current bean has property with [name]?
     */
    fun hasProperty(name: String): Boolean {
        return propertiesByName.containsKey(name)
    }

    /**
     * @return all properties in the current bean
     */
    fun getProperties(): Collection<P> = propertiesByName.values

    /**
     * @return all properties with getters in the current bean
     */
    fun getPropertiesWithGetter(): Collection<P> = getProperties().filter { it.getter() != null }

    /**
     * @return all properties with setters in the current bean
     */
    fun getPropertiesWithSetter(): Collection<P> = getProperties().filter { it.setter() != null }

    /**
     * @return all properties with public getters in the current bean
     */
    fun getPropertiesWithPublicGetter(): Collection<P> = getProperties().filter { p ->
        val getter = p.getter()
        getter != null && getter.isPublic()
    }

    /**
     * @return all properties with public setters in the current bean
     */
    fun getPropertiesWithPublicSetter(): Collection<P> = getProperties().filter { p ->
        val setter = p.setter()
        setter != null && setter.isPublic()
    }

}