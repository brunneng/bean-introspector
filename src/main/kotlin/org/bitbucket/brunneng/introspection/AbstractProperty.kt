package org.bitbucket.brunneng.introspection

import org.bitbucket.brunneng.introspection.property.*
import java.lang.reflect.Method

/**
 * Abstract property of bean class. Can be accessed with [AbstractBeanIntrospector.property].
 *
 * Gives access to detected *getter* and *setter* of property, so you can manipulate with data of beans in
 * generic reflective way.
 *
 * Extend it to provide custom metadata related to property
 *
 * @param name name of property
 * @param getters list of property getters
 * @param setters list of property setters
 * @param P class of introspected property
 * @param B class of bean introspector
 */
abstract class AbstractProperty<P : AbstractProperty<P, B>, B : AbstractBeanIntrospector<P, B>>(
    val name: String,
    protected val getters: List<Getter>,
    protected val setters: List<Setter>,
    private val beanIntrospector: AbstractBeanIntrospector<P, B>
) {

    val parentBeanProperty: P?

    protected var onlyPublic: Boolean = false

    protected var getter: Getter? = null
    protected var setter: Setter? = null

    init {
        for (getter in getters) {
            getter.accessorValueType.typeCategory = beanIntrospector.introspector.getTypeCategory(
                getter.accessorValueType.actualClass
            )
        }

        for (setter in setters) {
            setter.accessorValueType.typeCategory = beanIntrospector.introspector.getTypeCategory(
                setter.accessorValueType.actualClass
            )
        }

        val parent = beanIntrospector.parentBeanIntrospector
        parentBeanProperty = if (parent != null && parent.hasProperty(name)) {
            parent.property(name)
        } else {
            null
        }

        updateGetterAndSetter()
    }

    protected fun updateGetterAndSetter() {
        getter = findBestAccessor(getters)
        setter = findBestAccessor(setters)
    }

    private fun <T : Accessor> findBestAccessor(accessors: List<T>): T? {
        var res = accessors.sortedWith(compareBy({ it.isPublic() }, { it.isPublicOwner() },
            { it.fieldOrMethod is Method }, { !it.isSynthetic() })
        ).reversed()
        if (onlyPublic) {
            res = res.filter { it.isPublic() }
        }
        return res.firstOrNull { it.ensureAccessible() }
    }

    /**
     * Rebuilds introspector of given property to allow to use only public accessors.
     */
    @Synchronized
    open fun allowOnlyPublicAccessors() {
        if (onlyPublic) {
            return
        }

        onlyPublic = true
        updateGetterAndSetter()
    }

    /**
     * @return [getter][Getter] of this property, or *null*, if no suitable getter found
     */
    fun getter(): Getter? {
        return getter
    }

    /**
     * @return [setter][Setter] of this property, or *null*, if no suitable setter found
     */
    fun setter(): Setter? {
        return setter
    }

    /**
     * @return property description of given property. [AccessorValueType] is taken from getter, or setter, if
     * getter doesn't exist
     */
    fun getPropertyDescription(): PropertyDescription {
        val accessor: Accessor = getter() ?: setter()!!
        return PropertyDescription(beanIntrospector.beanClass, name, accessor.accessorValueType)
    }

    /**
     * Collects all annotations from all accessors, starting from public ones. Also annotations from getter
     * accessors are collected before setters. The collection could contain instance of the same annotation if it
     * has different parameters.
     * @return ordered collection of all annotation instances without duplications.
     */
    fun getAnnotations(): Collection<Annotation> {
        val res = LinkedHashSet<Annotation>()

        getters.filter { g -> g.isPublic() }.forEach {
            res.addAll(it.getAnnotations())
        }
        setters.filter { s -> s.isPublic() }.forEach {
            res.addAll(it.getAnnotations())
        }
        getters.filter { g -> !g.isPublic() }.forEach {
            res.addAll(it.getAnnotations())
        }
        setters.filter { s -> !s.isPublic() }.forEach {
            res.addAll(it.getAnnotations())
        }

        return res
    }

    override fun toString(): String {
        return "${beanIntrospector.beanClass.simpleName}.$name"
    }
}