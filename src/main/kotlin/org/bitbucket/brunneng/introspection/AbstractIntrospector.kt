package org.bitbucket.brunneng.introspection

import org.bitbucket.brunneng.introspection.exceptions.PathResolvingException
import org.bitbucket.brunneng.introspection.property.*
import org.bitbucket.brunneng.introspection.util.*
import java.lang.reflect.Type
import java.util.*
import java.util.concurrent.ConcurrentHashMap

/**
 * Abstract introspector which is a central point from which it's possible to introspect beans and their properties.
 *
 * Gives the ability to configure [ClassMatcher]s to detect what should be considered as value types, collections,
 * maps and beans.
 *
 * Extend from this class to create your custom bean introspectors and properties, with possible additional
 * configurations.
 *
 * @param P class of introspected property
 * @param B class of bean introspector
 *
 * @see AbstractProperty
 * @see AbstractBeanIntrospector
 */
abstract class AbstractIntrospector<P : AbstractProperty<P, B>, B : AbstractBeanIntrospector<P, B>> {

    protected val valueClassMatchers = ArrayList<ClassMatcher>()
    protected val collectionClassMatchers = ArrayList<ClassMatcher>()
    protected val mapClassMatchers = ArrayList<ClassMatcher>()

    internal val getterDetectors = ArrayList<PropertyAccessorDetector<out Getter>>()
    internal val setterDetectors = ArrayList<PropertyAccessorDetector<out Setter>>()
    internal val ignoredPropertiesMatchers = ArrayList<IgnoredPropertiesMatcher>()

    protected val beanConfigs = ConcurrentHashMap<Type, B>()

    protected val typeCategoryCache = ConcurrentHashMap<Class<*>, TypeCategory>()

    /**
     * Options to control nuances of assessors detection
     */
    val accessorsDetectionOptions: AccessorsDetectionOptions = AccessorsDetectionOptions()

    init {
        valueClassMatchers += IsEnumClassMatcher()
        valueClassMatchers += IsPrimitiveClassMatcher()
        valueClassMatchers += PackageNameStartsWithClassMatcher("java.lang")
        valueClassMatchers += PackageNameStartsWithClassMatcher("java.math")
        valueClassMatchers += PackageNameStartsWithClassMatcher("java.time")
        valueClassMatchers += IsAssignableClassMatcher(Date::class.java)
        valueClassMatchers += IsAssignableClassMatcher(Calendar::class.java)
        valueClassMatchers += IsAssignableClassMatcher(UUID::class.java)

        collectionClassMatchers += IsArrayClassMatcher()
        collectionClassMatchers += IsAssignableClassMatcher(MutableCollection::class.java)

        mapClassMatchers += IsAssignableClassMatcher(MutableMap::class.java)

        getterDetectors += BeanIsBooleanGetterDetector()
        getterDetectors += BeanGetterDetector()
        getterDetectors += FieldGetterDetector()

        setterDetectors += BeanSetterDetector()
        setterDetectors += FieldSetterDetector()

        ignoredPropertiesMatchers += GroovyIgnoredPropertiesMatcher()
    }

    /**
     * Value type is a kind of simple type like:
     * * primitive types and their wrappers
     * * java types in packages: java.lang, java.math, java.time
     * * types assignable to [Date] and [Calendar]
     *
     * This list can be extended by using [AbstractIntrospector.addValueType].
     *
     * @param testClass class which should be checked
     *
     * @return is given [testClass] value type?
     */
    fun isValueType(testClass: Class<*>): Boolean {
        synchronized(valueClassMatchers) {
            return valueClassMatchers.stream().anyMatch { m -> m.isMatched(testClass) }
        }
    }

    /**
     * Collection type is a kind of one dimensional storage of objects:
     * Default collections types are:
     * * Arrays
     * * Mutable collections, such as: [ArrayList], [HashSet] and others
     *
     * This list can be extended by using [AbstractIntrospector.addCollectionType].
     *
     * @param testClass class which should be checked
     *
     * @return is given [testClass] collection type?
     */
    fun isCollectionType(testClass: Class<*>): Boolean {
        synchronized(collectionClassMatchers) {
            return collectionClassMatchers.stream().anyMatch { m -> m.isMatched(testClass) }
        }
    }

    /**
     * Map type is a kind of storage with key-value semantics.
     * Default map types are:
     * * Mutable maps, such as: [HashMap], [TreeMap] and others
     *
     * New map types can be registered using [AbstractIntrospector.addMapType].
     *
     * @param testClass class which should be checked
     *
     * @return is given [testClass] map type?
     */
    fun isMapType(testClass: Class<*>): Boolean {
        synchronized(mapClassMatchers) {
            return mapClassMatchers.stream().anyMatch { m -> m.isMatched(testClass) }
        }
    }

    /**
     * Checks that given [testClass] is bean type. To get detailed explanation what bean types are, see
     * [AbstractBeanIntrospector].
     *
     * @param testClass class which should be checked
     *
     * @return is given [testClass] bean type?
     */
    fun isBeanType(testClass: Class<*>): Boolean {
        if (beanConfigs.containsKey(testClass)) {
            return true
        }
        if (testClass == Object::class.java) {
            return false
        }

        return !isValueType(testClass) && !isCollectionType(testClass) && !isMapType(testClass)
    }

    /**
     * @param testClass class which should be checked
     *
     * @return type category of [testClass]
     *
     * @see TypeCategory
     */
    fun getTypeCategory(testClass: Class<*>): TypeCategory {
        return typeCategoryCache.getOrPut(testClass) {
            when {
                isValueType(testClass) -> TypeCategory.Value
                isCollectionType(testClass) -> TypeCategory.Collection
                isMapType(testClass) -> TypeCategory.Map
                isBeanType(testClass) -> TypeCategory.Bean
                else -> TypeCategory.Undefined
            }
        }
    }

    /**
     * Registers a new value type, in addition to default value types.
     *
     * @param valueType type which should be considered as value type
     *
     * @see AbstractIntrospector.isValueType
     */
    fun addValueType(valueType: Class<*>) {
        synchronized(valueClassMatchers) {
            typeCategoryCache.clear()
            valueClassMatchers += IsAssignableClassMatcher(valueType)
        }
    }

    /**
     * Registers a new collection type, in addition to default collection types.
     *
     * @param collectionType type which should be considered as collection type
     *
     * @see AbstractIntrospector.isCollectionType
     */
    fun addCollectionType(collectionType: Class<*>) {
        synchronized(collectionClassMatchers) {
            typeCategoryCache.clear()
            collectionClassMatchers += IsAssignableClassMatcher(collectionType)
        }
    }

    /**
     * Registers a new map type, in addition to default map types.
     *
     * @param mapType type which should be considered as map type
     *
     * @see AbstractIntrospector.isMapType
     */
    fun addMapType(mapType: Class<*>) {
        synchronized(mapClassMatchers) {
            typeCategoryCache.clear()
            mapClassMatchers += IsAssignableClassMatcher(mapType)
        }
    }

    /**
     * Adds getter detector with highest priority.
     *
     * @param propertyGetterDetector getter detector
     *
     * @see PropertyAccessorDetector
     */
    fun addGetterDetector(propertyGetterDetector: PropertyAccessorDetector<Getter>) {
        getterDetectors.add(0, propertyGetterDetector)
    }

    /**
     * Adds setter detector with the highest priority.
     *
     * @param propertySetterDetector setter detector
     *
     * @see PropertyAccessorDetector
     */
    fun addSetterDetector(propertySetterDetector: PropertyAccessorDetector<Setter>) {
        setterDetectors.add(0, propertySetterDetector)
    }

    /**
     * Adds matcher of properties which should be ignored during introspection.
     *
     * @param ignoredPropertiesMatcher matcher of ignored properties
     *
     * @see IgnoredPropertiesMatcher
     */
    fun addIgnoredPropertiesMatcher(ignoredPropertiesMatcher: IgnoredPropertiesMatcher) {
        ignoredPropertiesMatchers += ignoredPropertiesMatcher
    }

    /**
     * @param beanClass class of bean, it mustn't be : Object.class, value type, collection type, map type.
     *
     * @return introspector for given [beanClass], it will be created, if doesn't exist yet, and cached. So subsequent
     * similar calls to this method will return the same instance.
     */
    fun beanOfClass(beanClass: Class<*>): B {
        return beanOfClass(beanClass, null)
    }

    /**
     * @param beanClass class of bean, it mustn't be : Object.class, value type, collection type, map type.
     * @param beanType optional bean type information, can be either [java.lang.reflect.ParameterizedType]
     * or [Class] equal to [beanClass].
     *
     * @return introspector for given [beanClass], it will be created, if doesn't exist yet, and cached. So subsequent
     * similar calls to this method will return the same instance.
     */
    fun beanOfClass(beanClass: Class<*>, beanType: Type?): B {
        val typeOrClass = beanType ?: beanClass
        val res = beanConfigs[typeOrClass]
        if (res != null) {
            return res
        }

        synchronized(beanConfigs) {
            return beanConfigs.getOrPut(typeOrClass) {
                createBean(typeOrClass)
            }
        }
    }

    /**
     * Get value inside bean by path, which has '.' as delimiter.
     *
     * For example if path is "address.city.population" it will resolve aggregated bean by "address", then will take
     * "city" in address, and then "population" of city.
     * @param bean source bean to get value in it
     * @param path path to the value
     * @param T expected type of value
     * @return value by path
     * @throws PathResolvingException if the path can't be resolved for some reason (see the message
     * and nested exceptions)
     */
    fun <T : Any?> getValue(bean: Any, path: String): T {
        val propertyResolvingResult = resolveLastProperty(bean, path)
        val lastProperty = propertyResolvingResult.property
        try {
            val getter = lastProperty.getter()
                ?: throw PathResolvingException("Can't resolve '$propertyResolvingResult': no getter for property")

            return getter.get(propertyResolvingResult.bean) as T
        } catch (e: Exception) {
            if (e !is PathResolvingException) {
                throw PathResolvingException("Can't resolve '${lastProperty.name}': ${e.message}", e)
            }
            throw e
        }
    }

    /**
     * Set value inside bean by path, which has '.' as delimiter.
     *
     * For example if path is "address.city.population" it will resolve aggregated bean by "address", then will take
     * "city" in address, and then will set "population" of city.
     * @param bean source bean to set value in it
     * @param path path to the field or property which should be updated
     * @throws PathResolvingException if the path can't be resolved for some reason (see the message
     * and nested exceptions)
     */
    fun setValue(bean: Any, path: String, value: Any?) {
        val propertyResolvingResult = resolveLastProperty(bean, path)
        val lastProperty = propertyResolvingResult.property
        try {
            val setter = lastProperty.setter()
                ?: throw PathResolvingException("Can't resolve '${lastProperty.name}': no setter for property")

            setter.set(propertyResolvingResult.bean, value)
        } catch (e: Exception) {
            if (e !is PathResolvingException) {
                throw PathResolvingException("Can't resolve '${lastProperty.name}': ${e.message}", e)
            }
            throw e
        }
    }

    private fun resolveLastProperty(bean: Any, path: String): PropertyResolvingResult<P> {
        val pathElements = path.split(".")
        var currentValue: Any? = bean
        val iterator = pathElements.iterator()
        while (iterator.hasNext()) {
            val element = iterator.next()
            try {
                if (currentValue == null) {
                    throw PathResolvingException("Can't resolve '$element': source object is null")
                }

                val bi = beanOfClass(currentValue::class.java)
                val property = bi.property(element)
                if (!iterator.hasNext()) {
                    return PropertyResolvingResult(currentValue, property)
                }

                val getter = property.getter()
                    ?: throw PathResolvingException("Can't resolve '$element': no getter for property")

                currentValue = getter.get(currentValue)
            } catch (e: Exception) {
                if (e !is PathResolvingException) {
                    throw PathResolvingException("Can't resolve '$element': ${e.message}", e)
                }
                throw e
            }
        }
        throw PathResolvingException("Can't resolve path '$path': unknown reason")
    }

    /**
     * @param beanType type of bean to introspect
     * @return new bean introspector of given [beanType]
     */
    protected abstract fun createBean(beanType: Type): B

    class PropertyResolvingResult<P>(val bean: Any, val property: P)
}