package org.bitbucket.brunneng.introspection

import org.bitbucket.brunneng.introspection.property.Getter
import org.bitbucket.brunneng.introspection.property.Setter
import java.lang.reflect.Type

/**
 * Default implementation of [abstract bean introspector][AbstractBeanIntrospector].
 */
open class BeanIntrospector internal constructor(
    beanType: Type,
    introspector: AbstractIntrospector<Property, BeanIntrospector>
) :
    AbstractBeanIntrospector<Property, BeanIntrospector>(beanType, introspector) {

    override fun createProperty(name: String, getters: List<Getter>, setters: List<Setter>): Property {
        return Property(name, getters, setters, this)
    }
}